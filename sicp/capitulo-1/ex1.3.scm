; Ejercicio 1.3: Diseña un proceso que tome tres número como argumentos y
; regrese la suma de los cuadrados de los dos número mayores.

(define (cuadrado x) (* x x))

(define (suma-cuadrados x y) (+ (cuadrado x) (cuadrado y)))

; sdcs = suma de dos cuadrados
(define (sdcs-mayores x y z)
  (cond 
    	((and (>= x z) (>= y z)) (suma-cuadrados x y))
  	((and (>= y x) (>= z x)) (suma-cuadrados y z))
	((and (>= x y) (>= z y)) (suma-cuadrados x z)))
  )

(if (= (sum-square-two-largest 1 1 2) 5) true false)
(if (= (sum-square-two-largest 2 1 3) 13) true false)
(if (= (sum-square-two-largest 2 3 1) 13) true false)
(if (= (sum-square-two-largest 1 1 1) 2) true false)
