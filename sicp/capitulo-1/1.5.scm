;  Ejercicio 1.5: Ben Bitdiddle ha
;  invetado una prueba para determinar
;  si el intérprete al que se enfrenta
;  está utilizando una evaluación de
;  orden aplicativo o una evaluacióna de
;  orden normal. Él define los
;  siguientes dos procedimiento:

(define (p) (p))

(define (test x y) 
  (if (= x 0) 
      0 
      y))

; Entonces evalua la expresión

(test 0 (p))

; ¿Qué comportamiento observará Ben con
; un interprete que usa evaluación orden
; aplicativo? ¿Qué comportamiento
; observará con un interprete que usa el
; orden normal de evaluación? Explica tu respuesta.

; Evaluación de orden aplicativo
; 
