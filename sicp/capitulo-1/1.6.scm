; Alyssa P. Hacker no ve porque `if` necesita ser dada por una forma especial.
; "¿Porqué no podemos solo definirla en terminos de `cond`s?" ella pregunta. La
; amiga de Alyssa Eva Lu Ator afirma que es posible hacerlo y define una nueva
; versión de `if` como:


(define (neo-if predicado 
                clausula-then 
                clausula-else)
  (cond (predicado clausula-then)
        (else clausula-else)))

; Eva demuestra el programa para Alyssa:

(neo-if (= 2 3) 0 5)
;5

(neo-if (= 1 1) 0 5)
;0

;Encantada, Alyssa usa `neo-if` para
;reescribir el programa `rzcda` 


(define (cuadrada-iter propuesto x)
  (neo-if (suficiente? propuesto x)
      propuesto
      (cuadrada-iter (mejorar propuesto x) x)))

;¿Qué pasa cuando Alyssa intenta usar
;esto para calcular raices cuadradas?
;Explica

;Respuesta:
;

