;1.8 El método de Newton para raices cubicas esta basado
;en el hecho de que si y es una aproximación de la raíz
;cúbica de x, entonces una mejor aproximación se da con
;la siguiente expresión ((x/y^2)+2*y)/3
;
; Usa la ecuación para implementar un proceso
; "raiz-cubica" analogo al proceso "raiz-cuadrada"
(define (cuadrado x) (* x x))

(define (cubo x) (* x x x))

(define (cubica-iter propuesto x)
  (if (suficiente? propuesto x)
      propuesto
      (cubica-iter (mejorar propuesto x) x)))

(define (mejorar y x)
  (/ (+ 
       (/ x 
	  (cuadrado y))
       (* 2 y)) 
     3))

(define (suficiente? propuesto x)
  (< (abs (- (cubo propuesto) x)) 0.001))

(define (rzcb x)
  (cubica-iter 1.0 x))

