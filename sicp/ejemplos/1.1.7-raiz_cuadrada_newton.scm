(define (cuadrado x) (* x x))

(define (cuadrada-iter propuesto x)
  (if (suficiente? propuesto x)
      propuesto
      (cuadrada-iter (mejorar propuesto x) x)))

(define (mejorar propuesto x)
  (promedio propuesto (/ x propuesto)))

(define (promedio x y) 
  (/ (+ x y) 2))

(define (suficiente? propuesto x)
  (< (abs (- (cuadrado propuesto) x)) 0.001))

(define (rzcda x)
  (cuadrada-iter 1.0 x))
