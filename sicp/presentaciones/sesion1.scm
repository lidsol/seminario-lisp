; Codigo de la sesion 1

(print "Hola mundo")

; Lista vacía
()

; Numeros
1

10

3.14

1.4e5

; Escapando comillas

(print "\"Hola mundo\"")

; Read-Eval-Print-loop (REPL)

;Errores
(5) ; 5 is not a function

; Objeto autoevaluador
"Hola"

#t

5 + 5

(5 + 5)

;Notación prefija o polaca
(+ 5 5)

(+ 5 6 7 8 9 0)

(+ 5 6 5 (- 5 8))

(- 8 5)

(string-length "Hola mundo cruel")

(and #t #f)

(or #t #f)

(not #f)

(not (or #t #f))

; cuadrado

; suma de cuadrados

; Meta suma de cuadrados
(define (f a) (suma-de-cuadrados (+ a 1) (* a 2)))


(define (abs x) 
  (cond ((> x 0) x)
        ((= x 0) 0)
        ((< x 0) (- x))
    )
)

(define (abs x)
  (cond ((< x 0) (- x))
        (else x)
    )
)

; Esta función devuelve el valor absoluto de un número entero
(define (abs x)
  (if (< x 0)
      (- x)
      x
  )
)

(define (factorial n)
  (if (= n 1)
      1
      (* n (factorial (- n 1)))
  )
)
