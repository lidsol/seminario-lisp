(define (sum numeros)
  (if
    (null? numeros) 0
    (+ (car numeros) (sum (cdr numeros)))
  )
)
