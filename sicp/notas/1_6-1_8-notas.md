# 1.6 Expresiones condicionales y predicados

* Pensemos en el proceso que devuelve el valor absoluto de un número ¿Podríamos realizarlo?
* Nuestros poderes para la definición de procesos porque no tenemos
	herramientas para hacer pruebas y decidir diferentes operaciones dependiendo
	de estas pruebas. 
* Esta notación se le llama case analysis y en Lisp tiene una forma de ser
	representada
	* Se llama `cond` (Por condicional). Veamos un ejemplo

```scheme
(define (abs x)
(cond ((> x 0) x)
			((= x 0) 0)
			((< x 0) (- x))))
```

* La forma general es

```scheme
(cond (<p1> <e1>)
		(<p2> <e2>)
		…
		(<pk> <ek>))
```
		
* Consiste en el simbolo `cond` seguido de un parentesis que contiene expresiones `(<p> <e>)`llamadas clausulas.
	* La primera parte de la clausula es un **predicado** (una expresión que es evaluada y devuelve cierto o falso)
	
## ¿Cómo se evaluan las expresiones condicionales?

1. El predicado  `< p1 >` es evaluado primero
2. Si el valor es falso, entonces se evalúa `< p2 >`
3. Si el valor de `< p2 >` también es falso, entonces `< pk >`es evaluado

* El proceso continua hasta que encuentre algún **predicado** cuyo valor sea
`true`. En tal caso, el interprete devolverá el valor que corresponde con
a la **expresión consecuente `< e >`**  de la clausula como valor de la
expresión condicional.
* Si ningún predicado es devuelve verdadero el valor de `cond` sera indefinido

## ¿PrediQuién?

* La palabra **predicado** se refiere a procedimientos que regresan `true` o `false`
* el procedimiento `abs` utiliza **predicados primitivos**

### Predicados primitivos

Toman dos números como argumento y comprueban que el primer número sea:

* `<` - Menor que el segundo número
* `>` - Mayor que el segundo número
* `=` - Igual que el segundo número

## Otra forma de resolver el problema

```scheme
(define (abs x)
  (cond ((< x 0) (- x))
        (else x)))
```

* En español sería algo como 

> Si *x* es menor que 0 devuelve *-x*; en otro caso devuelve *x*

* `else` es un símbolo especial que es usado como `< p >` final de la clausula `cond`
	* Este símbolo hace que el valor devuelto por `cond` sea el valor correspondiente `< e >` cuando todas las clausulas `< p >` anteriores fueron pasadas

## Una más

```scheme
(define (abs x)
  (if (< x 0)
      (- x)
      x))
```

* Utilizamos otra forma especial llamada `if` exclusiva para cuando tengamos dos casos

### Forma General

`(if <predicado> <consecuencia> <alternativa>)`

### Evaluación

1. el interprete comienza evaluando la parte del `<predicado>`
2. si el `<predicado>` devuelve `true` el interprete evalua la `<consecuencia>` y devuelve su valor
3. En otro caso, se evalua la `<alternativa>` y devuelve su valor

## Operadores de composición lógica

Además de los predicados primitivos `<`, `>` y `=`, están los operadores de
composición lógica que permiten crear predicados compuestos.
A continuación los más usados:

* `(and <e1> ... <ek>)`
	* El interprete evalúa las expresiones `<e>` una a la vez, **de izquierda a derecha**
	* si alguna de las expresiones `<e>` es `false` la expresión devuelve `falso` y el resto de expresiones no será evaluadas
	* Si todas las expresiones `<e>` son `true`, el valor de la expresion `and` sera la el valor de la ultima expresion 
* `(or <e1> ... <en>)`
	* El interprete evalúa las expresiones `<e>` una a la vez, **de izquierda a derecha**
	* si alguna de las expresiones `<e>` es `true`, ese valor sera el valor devuelto por `or`. El resto de expresiones no es evaluado 
	* Si todas las expresiones `<e>` son `false`, el valor de la expresion `or` sera `false`
* `(not <e>)`
	* El valor del operador `not` será `true` cuando la expresion `<e>` devuelva `false`, y `false` en caso contrario 

### Ejemplos

1. Sea un número $x$, verificar si el número se encuentra en un rango entre 5 y 10

`(and (> x 5) (< x 10))`

2. Verificar si un número es mayor o igual a otro

```scheme
(define (>= x y) 
  (or (> x y) (= x y)))
```

O una alternativa:

```scheme
(define (>= x y) 
  (not (< x y)))
```

# 1.7 Raíces cuadradas por método de Newton

* Los procesos definidos hasta ahora se parecen mucho a las funciones matemáticas. Ellas especifican un valor dado uno o mas parámetros.
Sin embargo, hay una importante diferencia entre  las funciones matemáticas y
los procesos computacionales. Los procesos deben ser efectivos.

* Consideremos el problema de computar una raíz cuadrada. Podríamos definir la función como:

> $\sqrt{x} = y$ tal que $y \geq 0$ y $y^2=x$

* Esto describe perfectamente una función matemática. Podemos  utilizarla para
reconocer si un número es la raíz cuadrada de otro, o encontrar hechos sobre
raíces cuadradas en general.
* Por otro lado, la definición no describe un proceso. En realidad, no dice nada sobre como encontrar la raiz cuadrada de un número dado.
* Definir el proceso en pseudo-Lisp parece no ayudar

```scheme
(define (sqrt x)
  (the y (and (>= y 0) 
              (= (square y) x))))
```

## Contraste entre funciones y procesos

* Una analogía entre funciones y procesos puede ser la de describir propiedades
y describir como hacer cosas (distinción entre conocimiento imperativo y declarativo)
	* En matemáticas generalmente nos interesa lo declarativo (¿Qué es?) de las descripciones
	* En ciencias de computación interesa lo imperativo (¿Cómo se hace?) de las descripciones

## ¿**Cómo** computar la raíz cuadrada?

* Una forma común de computar la raíz cuadrada de un número es por el metodo de aproximaciones sucesivas (Método de Newton)

### Método

1. Proponemos un número $y$ como solución a la raíz cuadrada de otro número $x$ 
2. Realizamos el promedio entre $y$ y $\frac{x}{y}$ para obtener una solución
cada vez mejor (más cercana a la raíz cuadrada real)

|Numero propuesto (y)| Cociente (x/y)|Promedio            |
|--------------------|---------------|--------------------|
|1                   |   (2/1)  = 2  |  ((2 + 1)/2) = 1.5 |
|1.5                 |(2/1.5) = 1.3333|((1.3333 + 1.5)/2) = 1.4167|
|1.4167							 |(2/1.4167) = 1.4118|((1.4167 + 1.4118)/2) = 1.4142|
|1.4142              |...            |   ...              |

Continuar con este proceso nos dará una aproximación cada vez mejor de la raíz cuadrada

## Formalizando el proceso

1. Comenzaremos un valor para el *radicando* (el número del cual queremos
obtener la raíz cuadrada) y un valor para nuestro numero propuesto $y$
	* Si $y$ es suficientemente bueno para nuestros propositos terminamos
	* Si no lo es, debemos repetir el proceso para mejorar $y$

```scheme
(define (sqrt-iter guess x)
  (if (good-enough? guess x)
      guess
      (sqrt-iter (improve guess x) x)))
```

2. $y$ es mejorado por el promedio del *radicando* y la $y$ pasada

```scheme
(define (improve guess x)
  (average guess (/ x guess)))
```

3. Dónde

```scheme
(define (average x y) 
  (/ (+ x y) 2))
```

4. Además debemos definir `good-enough?`. La definición siguiente es meramente
ilustrativa ya que no es del todo una buena prueba (mira el ejercicio 1.7).
	* La idea es mejorar la respuesta hasta que a la diferencia del cuadrado de
	la respuesta y el radicando tengan una diferencia cercana a una tolerancia
	determinada (en este caso `0.001`)

```scheme
(define (good-enough? guess x)
  (< (abs (- (square guess) x)) 0.001))
```

#### Nota: Recuerda agregar la definición de `square` a tu entorno

5. Finalmente necesitamos comenzar con algún número como propuesta. Podemos
proponer que la raíz cuadrada de cualquier número siempre será `1`

```scheme
(define (sqrt x)
  (sqrt-iter 1.0 x))
```

Ahora podemos usar nuestra `sqrt` como cualquier proceso:

```scheme
(sqrt 9)
3.00009155413138

(sqrt (+ 100 37))
11.704699917758145

(sqrt (+ (sqrt 2) (sqrt 3)))
1.7739279023207892

(square (sqrt 1000))
1000.000369924366
```

Nuestro proceso `sqrt` muestra como con el lenguaje procedural simple que hemos
abordado hasta ahora es suficiente para escribir cualquier programa puramente
numérico que podría ser escrito en otros lenguajes como `C` o `Python`. Esto
parece sorprendente ya que que no hemos incluido en nuestro lenguaje ningún a
construcción iterativa (ciclos) que le indiquen directamente a la computadora
que debe ejecutar acciones una y otra vez. `sqrt-iter`, por otra parte,
demuestra como las iteraciones pueden ser realizadas usando solamente la
habilidad ordinaria de llamar procesos
