; Suma uno a un número
(define (add1 n)
  (+ n 1))

; Resta uno a un número
(define (sub1 n)
  (- n 1))

; Función para condición de paro en números
(zero? 0)

; Suma recursiva
(define (o+ n m)
  (cond 
    ((zero? m) n)
    (else (add1 (o+ n (sub1 m))))))

; Resta recursiva
(define (o- n m)
   (cond 
     ((zero? m) n)
     (else (sub1 (o- n (sub1 m))))))

; Suma de los elementos de una tupla
(define (addtup tup)
  (cond 
    ((null? tup) 0)
    (else (o+ (car tup) (addtup (cdr tup))))))

; Multiplicación
(define (x n m)
  (cond 
    ((zero? m) 0)
    (else (o+ n (x n (sub1 m))))))

; Suma elemento a elemento
(define (tup+ tup1 tup2)
  (cond ((and (null? tup1) (null? tup2)) '())
	(else (cons (o+ (car tup1) (car tup2))
		    (tup+ (cdr tup1) (cdr tup2))))))

; Simplificando
(define (tup++ tup1 tup2)
  (cond 
    ((null? tup1) tup2)
    ((null? tup2) tup1)
    (else 
      (cons 
	(o+ (car tup1) (car tup2))
	(tup++ (cdr tup1) (cdr tup2))))))

; Operadores de relacion
(define (> n m)
  (cond 
    ((zero? m) #t)
    ((zero? n) #f)
    (else (> (sub1 n) (sub1 m)))))
; Falla con (> 3 3) -> #t

; Version correcta
(define (> n m)
  (cond 
    ((zero? n) #f)
    ((zero? m) #t)
    (else (> (sub1 n) (sub1 m)))))
;(> 6 6) -> #f

(define (< n m)
  (cond 
    ((zero? m) #f)
    ((zero? n) #t)
    (else (< (sub1 n) (sub1 m)))))

(define (= n m)
  (cond 
    ((zero? m) (zero? n))
    ((zero? n) #f)
    (else (= (sub1 n) (sub1 m)))))

; Re escrito en terminos de < y >
(define (= n m)
  (cond 
    ((> n m) #f)
    ((< n m) #f)
    (else #t)))

; Potencia
(define (pow n m)
  (cond 
    ((zero? m) 1)
    (else 
      (x n (pow n (sub1 m))))))

; Division
(define div
  (lambda (n m)
    (cond 
      ((< n m) 0)
      (else (add1 (div (o- n m) m))))))

; Longitud de una lista
(define (lenght lat)
  (cond 
    ((null? lat) 0)
    (else
      (add1 (lenght (cdr lat))))))

; cer0 ppick. Incorrecto
(define ppick
  (lambda (n lat)
          (cond
            ((zero? n) #f)
            (else (cond 
                    ((= (length lat) n) (car lat))
                    (else (ppick n (cdr lat))))))))

; pick: Obtiene un elemento n de una lista lat
(define (pick n lat)
  (cond 
    ((zero? (sub1 n)) (car lat))
    (else 
      (pick (sub1 n) (cdr lat)))))

; rempick: Obtiene una lista sin el elemento n
(define (rempick n lat)
  (cond 
    ((zero? (sub1 n)) (cdr lat))
    (else 
      (cons (car lat)
	    (rempick (sub1 n) (cdr lat))))))

; No numbs: Obtiene una lista quitando los numeros
(define (no-nums lat)
  (cond 
    ((null? lat) '())
    (else (cond 
	    ((number? (car lat)) (no-nums (cdr lat)))
	    (else 
	      (cons (car lat)
		    (no-nums (cdr lat))))))))

; All numbs: Obtiene una tupla de numero a partir de una lista
(define (all-nums lat)
  (cond 
    ((null? lat) '())
    (else 
      (cond 
	((number? (car lat)) (cons (car lat) (all-nums (cdr lat))))
	(else (all-nums (cdr lat)))))))

; Igualdad entre atomos
(define (eqan? a1 a2)
  (cond 
    ((and (number? a1) (number? a2)) (= a1 a2))
    ((or (number? a1) (number? a2)) #f)
    (else (eq? a1 a2))))

; occur: Cuenta ocurrencias de atomo en lista
(define (occur a lat)
  (cond 
    ((null? lat) 0)
    (else (cond 
	    ((eq? (car lat) a) 
	     (add1 (occur a (cdr lat))))
	    (else (occur a (cdr lat)))))))

; Saber si un numero es uno
(define (one? n)
  (= n 1))

; Redefiniendo rempick para usar one
(define (rempick n lat)
  (cond 
    ((one? n) (cdr lat))
    (else (cons 
	    (car lat)
	    (rempick (sub1 n) (cdr lat))))))
