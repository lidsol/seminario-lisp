# Consideraciones

* Operaremos, crearemos y modificaremos **Números**
* Solo trabajaremos con número enteros positivos: 0, 1, 2, 3, 4, ...
* Operaciones básicas
	* `(add1 n)`, `(sub1 n)`, `(zero? n)`
	```scheme
	(define (add1 n)
				(+ n 1))
	(define (sub1 n)
				(- n 1))
	(zero? 0) 
	```

# Operaciones

## Suma recursiva

* El *primer mandamiento* es sustituido en lugar de `null?` usamos `zero?`
* `add1` es con lo que construimos
	* Como `cons` es para construir listas

```scheme
(define (o+ n m)
	(cond 
		((zero? m) n)
		(else 
			(add1 (o+ n (sub1 m))))))
```

## Resta recursiva

* Similar a la suma

```scheme
(define (o+ n m)
	(cond 
		((zero? m) n)
		(else 
			(sub1 (o- n (sub1 m))))))
```

## Tuplas

* Las tuplas solo estan compuestas solo por numeros
* `addtup` crea un número sumando la totalidad de números de una tupla
* `+` es la forma de construir numeros 
	* Asi como `cons` construye listas
* El valor terminal para construir numeros es `0` 
	* Asi como `()` es el terminal de la construccion de listas
* Condicion de paro es `((null? tup) 0)` como la condicion para listas es `((null? l) ('()))`
* Las tuplas estan compuestas de una lista vacia o una lista que contiene un
  numero y el resto que es tambien una tupla La recursion con las tuplas y
  listas es la misma `(cdr tup)`
* Solo se hacen dos preguntas cobre las listas y tambien sobre las tuplas
	* `null?` y `else`
* `addtup`
	* Condicion de paro `((null? tup) 0)`
	* Recursion `(addtup (cdr tup))`
	* Operacion para Construir numeros `+`

```scheme
(define (addtup tup)
	(cond ((null? tup) 0)
				(else (+ (car tup) (addtup (cdr tup))))))
```

**Nota:** Similitud con `(cons (car lat) (rember a (cdr lat)))`

## Multiplicación

* La multiplicacion de n y m es sumar m veces el numero m
* La condicion de paro es `zero?`
	* Entonces debemos ir reduciendo el numero e uno `sub1`
* `(x n (sub1 m))` sera la condicion de paro

```scheme
(define (x n m)
	(cond 
		((zero? m) 0)
		(else 
			(o+ n (x n (sub1 m))))))
```

### Cuarto mandamiento (primera revisión)

Siempre cambiar al menos un argumento mientras hacemos recursión. Debe cambair
para estar mas cerca de la terminación. El argumento cambiado debe ser probado
en la condicion de termino:

* Al usar `cdr`, la condición de termino se prueba con `null?`
* Al usar `sub1`, la condicion de termino se prueba con `zero?`

### Corriendo un ejemplo

* Notar que `n` se le aplica la operacion `+ed` `m` veces
* ¿Porqué 0 es el valor terminal? Porqué `0` no afecta la operacion `+`.
`n + 0 = n`

### Cuarto mandamiento

* Cuando construyes un valor con `+`, siempre usa `0` como el valor de termino,
ya que sumar `0` no cambia el valor de la adición
* Cuando construyes un valor con `x`, siempre usa `1` como el valor de termino,
ya que multiplicar por `1` no modifica el valor del producto
* Cuando construyes un valor con `cons`, siempre considera `()` como valor de
termino.

## Operacion Tupla: Suma elemento a elemento `tup+`

* Suma el primer elemento de la `tupla1` con el primer elemento de la `tupla2`,
	el segundo elemento de la `tupla1` con el segundo elemento de la `tupla2` y
	asi sucesivamente
* Recursion doble?
* Hacemos 4 preguntas ya que tenemos dos tuplas que recorrer
	1. la primera tupla esta vacia o 2. no
	3. La segunda tupla esta vacia o 4. no

```scheme
(define (tup+ tup1 tup2)
	(cond ((and (null? tup1) (null? tup2)) '())
				(else 
					(cons (+ (car tup1) (car tup2))
						(tup+ (cdr tup1) (cdr tup2))))))
```

* Los argumentos de `o+` es `car tup1` y `car tup2`
* ¿Los argumentos del `cons`

```scheme
(cons (+ (car tup1) (car tup2)))

(tup+ (cdr tup1) (cdr tup2))
```

### Extendiendo el comportamiento

* ¿Qué pasa si damos los valores `(3 7)` y `(4 6 8 1)`?
	* No hay respuesta por que *tup1* se vuelve `null` antes que *tup2*
* Pero queremos como resultado `(7 13 8 1)`
* Reescribiendo `tup+` para que funcione aun si las tuplas son de longitudes
	distintas
	* ¿Si `tup2` es mas grande que `tup1`? `((null? tup1) tup2)`
	* ¿Si `tup1` es mas grande que `tup2`? `((null? tup2) tup1)`

* Re escribiendo

```scheme
(define (tup+ tup1 tup2)
	(cond 
		((and (null? tup1) (null? tup2)) '())
		((null? tup1) tup2)
		((null? tup2) tup1)
		(else 
			(cons (o+ (car tup1) (car tup2))
						(tup+ (cdr tup1) (crd tup2))))))
```

* Simplificando

```scheme
(define (tup+ tup1 tup2)
	(cond 
		((null? tup1) tup2)
		((null? tup2) tup1)
		(else 
			(cons (o+ (car tup1) (car tup2))
						(tup+ (cdr tup1) (crd tup2))))))
```

# Operaciones de relacion

## Operador mayor que

* Tenemos dos números para hacer la recursión `n` y `m`
* Debemos restar ambos numero hasta que alguno sea cero 
* Debemos hacer tres preguntas
	* `(zero? n)`, `(zero? m)` y `else`

```scheme
(define (> n m)
  (cond 
    ((zero? m) #t)
    ((zero? n) #f)
    (else (> (sub1 n) (sub1 m)))))
```

* El orden de las condiciones de paro es importante
* Version corregida

```scheme
(define (> n m)
  (cond 
    ((zero? m) #t)
    ((zero? n) #f)
    (else (> (sub1 n) (sub1 m)))))
```

### Operador meno que

* Mismas condiciones que el anterior

```scheme
(define (n m)
  (cond 
    ((zero? m) #f)
    ((zero? n) #t)
    (else (< (sub1 n) (sub1 m)))))
```

### Operador de equivalencia

* Se hacen tres preguntas
	* Si ambos llegan a cero al mismo tiempo
	* Si solo uno llega a cero
	* Recursion

```scheme
(define (= n m)
  (cond 
    ((zero? m) (zero? n))
    ((zero? n) #f)
    (else (= (sub1 n) (sub1 m)))))
```

* Re escrito en terminos de < y >

```scheme
(define (= n m)
  (cond 
    ((> n m) #f)
    ((< n m) #f)
    (else #t)))
```

## Potencia

```scheme
(define (pow n m)
  (cond 
    ((zero? m) n)
    (else 
      (x n (pow n (sub1 m))))))
```

## Division

* ¿Cuantas veces cabe el segundo numero en el primero?

```scheme
(define (div n m)
	(cond
		((< n m) 0)
		(else (add1 (div (- n m) m)))))
```

## Numero y listas

### lenght

* Longitud de una lista
* `(japon con jamon) = 3`

```scheme
(define (lenght lat)
  (cond 
    ((null? lat) 0)
    (else
      (add1 (lenght (cdr lat))))))
```

### Pick

* Obtener el elemento `n` de una lista

```scheme
(define (pick n lat)
  (cond 
    ((zero? (sub1 n)) (car lat))
    (else 
      (pick (sub1 n) (cdr lat)))))
```

* `(papas con quesito)` con `n = 3` daría `quesito`

### Rempick

* Elimina el elemento `n` de una lista `lat`

```scheme
(define (rempick n lat)
  (cond 
    ((zero? (sub1 n)) (cdr lat))
    (else 
      (cons (car lat)
```

### nonums

* eliminar todos los numeros de una lista
	* Usar `(number? n)`

```scheme
(define (no-nums lat)
  (cond 
    ((null? lat) '())
    (else (cond 
						((number? (car lat)) (no-numbs (cdr lat)))
						(else 
							(cons (car lat)
							(no-numbs (cdr lat))))))))
```

* `(2 manzanas 3 mangos y 10 guayabas)` = `(manzanas mangos y guayabas)`

### all-nums

```scheme
(define (all-nums)
  (cond 
    ((null? lat) '())
    (else 
      (cond 
				((number? (car lat))
				(cons (car lat) (all-nums (cdr lat))))
					(else (all-nums (cdr lat)))))))
```

### eqan?

* Verifica si un atomo es igual a otro

```scheme
(define (eqan? a1 a2)
  (cond 
    ((and (number? a1) (number? a2)) (= a1 a2))
    ((or (number? a1) (number? a2) #f))
    (else (eq? a1 a2))))
```

### occur

* Cuenta el numero de ocurrencias del atomo `a` en `lat`

```scheme
(define (occur a lat)
  (cond 
    ((null? lat) 0)
    (else (cond 
	    ((eq? (car lat) a) 
	     (add1 (occur a (cdr lat))))
	    (else (occur a (cdr lat)))))))
```

### One

* Verificar si un numero es igual a uno

```scheme
(define (one? n)
  (= n 1))
```

* Refinando `rempick` usando `one?` 

```scheme
(define (rempick n lat)
  (cond 
    ((one? n) (cdr lat))
    (else (cons 
	    (car lat)
	    (rempick (sub1 n) (cdr lat))))))
```
