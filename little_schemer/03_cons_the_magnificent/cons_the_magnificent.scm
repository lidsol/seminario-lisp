; Paul Aguilar @penserbjorne
; 2020/07/15

(define atom?
  (lambda (x)
    (and
      (not (pair? x))
      (not (null? x))
    )
  )
)

;(atom? (quote ())) ; Función de apoyo

;(quote atom) ; Evalua el atomo

;(car '(a b c)) ; Obtiene el primer elemento de la lista

;(cdr '(a b c)) ; Obtiene la sublista resultante de eliminar
                ; el primer elemento

;(cons 'a '(b c d)) ; Agrega una s-expression al inicio de la lista

;(null? '()) ; Esta vacia la lista?

;(atom? 'a) ; Es un atomo?

;(eq? 'a 'a) ; Son el mismo atomo no numerico?

(define lat?
  (lambda (l)
    (cond
      ((null? l) #t)
      ((atom? (car l)) (lat? (cdr l)))
      (else #f)
    )
  )
)

;(lat? '(a b c) ; La lista es de atomos?

(define member?
  (lambda (a lat)
    (cond
      ((null? lat) #f)
      (else
        (or
          (eq? (car lat) a)
          (member? a (cdr lat))
        )
      )
    )
  )
)

;(member? 'd '(a b c d)) ; Verifica si un elemento pertenece a la lista

(define rember
  (lambda (a lat)
    (cond
      ((null? lat) (quote ()))
      (else
        (cond
          ((eq? (car lat) a) (cdr lat))
          (else (rember a (cdr lat))
          )
        )
      )
    )
  )
)

;(rember 'c '(a b c a b c a b c))

(define rember2
  (lambda (a lat)
    (cond
      ((null? lat) (quote ()))
      (else
        (cond
          ((eq? (car lat) a) (cdr lat))
          (else (cons (car lat) (rember2 a (cdr lat)))
          )
        )
      )
    )
  )
)

;(rember2 'c '(a b c a b c a b c))

(define rember3
  (lambda (a lat)
    (cond
      ((null? lat) (quote ()))
      ((eq? (car lat) a) (cdr lat))
      (else
        (cons (car lat) (rember3 a (cdr lat)))
      )
    )
  )
)

;(rember3 'c '(a b c a b c a b c))

(define firsts
  (lambda (l)
    (cond
      ((null? l) (quote ()))
      (else
        (cons (car (car l)) (firsts (cdr l)))
      )
    )
  )
)

;(firsts '((a b) (c d) (e f)))

(define insertR
  (lambda (new old lat)
    (cond
      ((null? lat) (quote()))
      (else
        (cond
          ((eq? (car lat) old) (cons old (cons new (cdr lat))))
          (else
            (cons (car lat) (insertR new old (cdr lat)))
          )
        )
      )
    )
  )
)

;(insertR 'topping 'fudge '(ice cream with fudge for dessert))

(define insertL
  (lambda (new old lat)
    (cond
      ((null? lat) (quote()))
      (else
        (cond
          ;((eq? (car lat) old) (cons new (cons old (cdr lat))))
          ((eq? (car lat) old) (cons new lat))
          (else
            (cons (car lat) (insertL new old (cdr lat)))
          )
        )
      )
    )
  )
)

;(insertL 'topping 'fudge '(ice cream with fudge for dessert))

(define subst
  (lambda (new old lat)
    (cond
      ((null? lat) (quote()))
      (else
        (cond
          ((eq? (car lat) old) (cons new (cdr lat)))
          (else
            (cons (car lat) (subst new old (cdr lat)))
          )
        )
      )
    )
  )
)

;(subst 'topping 'fudge '(ice cream with fudge for dessert))

(define subst2
  (lambda (new o1 o2 lat)
    (cond
      ((null? lat) (quote()))
      (else
        (cond
          ((eq? (car lat) o1) (cons new (cdr lat)))
          ((eq? (car lat) o2) (cons new (cdr lat)))
          (else
            (cons (car lat) (subst2 new o1 o2 (cdr lat)))
          )
        )
      )
    )
  )
)

;(subst2 'vanilla 'chocolate 'banana '(banana ice cream with chocolate topping))

(define subst3
  (lambda (new o1 o2 lat)
    (cond
      ((null? lat) (quote()))
      (else
        (cond
          ((or (eq? (car lat) o1) (eq? (car lat) o2)) (cons new (cdr lat)))
          (else
            (cons (car lat) (subst3 new o1 o2 (cdr lat)))
          )
        )
      )
    )
  )
)

;(subst3 'vanilla 'chocolate 'banana '(banana ice cream with chocolate topping))

(define multirember
  (lambda (a lat)
    (cond
      ((null? lat) (quote()))
      (else
        (cond
          ((eq? (car lat) a) (multirember a (cdr lat)))
          (else
            (cons (car lat) (multirember a (cdr lat)))
          )
        )
      )
    )
  )
)

;(multirember 'cup '(coffe cup tea cup and hick cup))

(define multiinsertR
  (lambda (new old lat)
    (cond
      ((null? lat) (quote()))
      (else
        (cond
          ((eq? old (car lat))
            (cons old (cons new (multiinsertR new old (cdr lat))))
          )
          (else
            (cons (car lat) (multiinsertR new old (cdr lat)))
          )
        )
      )
    )
  )
)

;(multiinsertR 'tambien 'esto '(esto es una lista y esto es otra lista))

(define multiinsertL
  (lambda (new old lat)
    (cond
      ((null? lat) (quote()))
      (else
        (cond
          (
            (eq? old (car lat))
            (cons new (cons old (multiinsertL new old (cdr lat))))
          )
          (else
            (cons old (multiinsertL new old (cdr lat)))
          )
        )
      )
    )
  )
)

;(multiinsertL 'tambien 'esto '(esto es una lista y esto es otra lista)

(define multisubs
  (lambda (new old lat)
    (cond
      ((null? lat) (quote()))
      (else
        (cond
          ((eq? old (car lat))
            (cons new (multisubs new old (cdr lat)))
          )
          (else
            (cons (car lat) (multisubs new old (cdr lat)))
          )
        )
      )
    )
  )
)

;(multisubs 'Paul 'Paco '(Paco Jr Segundo es hijo de Paco Aguilar II y nieto de Paco Aguilar I))

;https://repl.it/repls/GleamingPhonyBits#main.scm
