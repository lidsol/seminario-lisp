(define atom?
    (lambda (x)
        (and (not (pair? x)) (not  (null? x)))))

(define add1
    (lambda (n)
        (+ n 1)))

(define sub1
    (lambda (n)
        (- n 1)))

; Multiplicación
(define (× n m)
  (cond
    ((zero? m) 0)
    (else (+ n (× n (sub1 m))))))

; Potencia
(define ↑
    (lambda (n m)
    (cond
        ((zero? m) 1)
        (else (× n (↑ n (sub1 m)))))))

; operacion := + | × | ↑
; aexpr := atomo | aexpr operacion aexpr
; (a + (b + c))

; nexpr := numero | nexpr operacion nexpr
; nexpr = 1 -> True
; nexpr = (3 + (4 × 5)) -> True
; nexpr = (2 × sausage) -> False
(define numbered?
    (lambda (aexpr)
        (cond
            ((atom? aexpr) (number? aexpr))
            ((eq? (cadr aexpr) (quote +))
                (and (numbered? (car aexpr)) (numbered? (caddr aexpr))))
            ((eq? (cadr aexpr) (quote ×))
                (and (numbered? (car aexpr)) (numbered? (caddr aexpr))))
            ((eq? (cadr aexpr) (quote ↑))
                (and (numbered? (car aexpr)) (numbered? (caddr aexpr)))))))

(numbered? 1) ; #t
(numbered? '(3 + (4 × 5))) ; #t
(numbered? '(2 × sausage)) ; #f

(define numbered?
    (lambda (aexpr)
        (cond
            ((atom? aexpr) (number? aexpr))
            (else
                (and (numbered? (car aexpr)) (numbered? (caddr aexpr)))))))

(numbered? 1) ; #t
(numbered? '(3 + (4 × 5))) ; #t
(numbered? '(2 × sausage)) ; #f

; atomo > numero

; operacion := + | × | ↑
; nexpr := numero | nexpr operacion nexpr
; nexpr = 1 -> True
; nexpr = (3 + (4 × 5)) -> True

(define value
    (lambda (nexpr)
        (cond
            ((atom? nexpr) nexpr) ; numero
            ((eq? (cadr nexpr) (quote +)) ; nexpr + nexpr
                (+ (value (car nexpr)) (value (caddr nexpr))))
            ((eq? (cadr nexpr) (quote ×)) ; nexpr + nexpr
                (× (value (car nexpr)) (value (caddr nexpr))))
            ((eq? (cadr nexpr) (quote ↑)) ; nexpr + nexpr
                (↑ (value (car nexpr)) (value (caddr nexpr)))))))


(value 13) ; -> 13
(value '(1 + (3 ↑ 4))) ; -> 82
(value 'cookie) ; -> ub

; nexpr := numero | operacion nexpr nexpr

(define value
    (lambda (nexpr)
        (cond
            ((atom? nexpr) nexpr) ; numero
            ((eq? (operator nexpr) (quote +)) ; nexpr + nexpr
                (+ (value (1st-sub-expr nexpr)) (value (2nd-sub-expr nexpr))))
            ((eq? (operator nexpr) (quote ×)) ; nexpr + nexpr
                (× (value (1st-sub-expr nexpr)) (value (2nd-sub-expr nexpr))))
            ((eq? (operator nexpr) (quote ↑)) ; nexpr + nexpr
                (↑ (value (1st-sub-expr nexpr)) (value (2nd-sub-expr nexpr)))))))

(value 13) ; -> 13
(value '(+ 1 (↑ 3 4))) ; -> 82
(value '(+ 3 (↑ 2 (× 2 2))))


(define 1st-sub-expr
    (lambda (expr)
        (cadr expr)))

(define 2nd-sub-expr
    (lambda (expr)
        (caddr expr)))

(define operator
    (lambda (expr)
        (car expr)))

; nexpr := numero | nexpr nexpr operacion
(value 13) ; -> 13
(value '(1 (3 4 ↑) +)) ; -> 82

(define 1st-sub-expr
    (lambda (expr)
        (car expr)))

(define 2nd-sub-expr
    (lambda (expr)
        (cadr expr)))

(define operator
    (lambda (expr)
        (caddr expr)))


'() ; zero

'(() () () ()) ; 4
'(()) ; 1
'(() ())

(define sero?
    (lambda (n)
        (null? n)))

(define edd1
    (lambda (n)
        (cons (quote ()) n)))

(define zub1
    (lambda (n) (cdr n)))


(define o+
    (lambda (n m)
        (cond
            ((sero? m) n)
            (else (edd1 (o+ n (zub1 m)))))))

(define lat?
    (lambda (l)
        (cond
            ((null? l) #t)
            ((atom? (car l)) (lat? (cdr l)))
            (else #f))))

() ; sero?

(define (edd1 n)
    (cons n '()))

(define (zub1 n)
    (car n))

; nexpr := numero | nexpr nexpr operacion

; (define value
;     (lambda (nexpr)
;         (cond
;             ((atom? nexpr) nexpr) ; numero
;             ((eq? (caddr nexpr) (quote +)) ; nexpr + nexpr
;                 (+ (value (car nexpr)) (value (cadr nexpr))))
;             ((eq? (caddr nexpr) (quote ×)) ; nexpr + nexpr
;                 (× (value (car nexpr)) (value (cadr nexpr))))
;             ((eq? (caddr nexpr) (quote ↑)) ; nexpr + nexpr
;                 (↑ (value (car nexpr)) (value (cadr nexpr)))))))
