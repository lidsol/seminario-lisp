(defun aoc-dia-01-1 (archivo)
  "Encuentra el mayor número de calorias de un grupo de alimentos"
  (with-open-file (s archivo)
    (do* ((linea (read-line s nil 'eof)
                (read-line s nil 'eof))
         (suma-bloque 0
                      (cond ((eq linea 'eof) suma-bloque)
                            ((string-equal linea "") 0)
                            (t (+ suma-bloque (parse-integer linea)))))
         (valor-maximo 0 (cond ((eq linea 'eof) valor-maximo)
                               ((> suma-bloque valor-maximo) suma-bloque)
                               (t valor-maximo))))
        ((eq linea 'eof) (return valor-maximo))
      ; (print suma-bloque) ; Descomentar para más info
      (print valor-maximo))))

(aoc-dia-01-1 "input1")
