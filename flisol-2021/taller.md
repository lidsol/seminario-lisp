# Taller de Intro a Lisp

## 1. Datos

* Hablar de atomos
* Predicados sencillos
	* `null`
	* `equal`
* Funciones
	* `car` 
	* `cons`

## 2. Listas

* Elementos de listas
	* apuntadores
* Funciones combinadas
	* cadr
	* cdar

## 3. Funciones

## 4. Condicionales

## 5. Recursion
