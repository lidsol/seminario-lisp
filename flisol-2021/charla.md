# Charla

**Objetivo**: comunicar nuestra experiencia aprendiendo a pensar en `lisp`.

## Paradigmas de programación - 5 (Diego)

* Imperativo
* OOP
* Funcional
* Declarativo/Logico

## Introducción LISP - 10 (Diego, cer0)

* Historia
	*  Origenes
	*  Auge
	*  El in(f)vierno de LISP
	*  El nuevo amanecer de LISP
* Características del lenguaje
	* Simbolos
	* Listas
	* Funciones
		* Condicionales

## Dejar de pensar en forma estructurada - 10 (cer0)

* Poner código equivalente en distintos lenguajes y en lisp
	* intro a recursión (pares o impares)
	* Tipos de recursión (fib)
	* programación aplicativa (diccionario otomi/español)

## Comunidad - 5 (Diego)

* ¿Como vamos aprendiendo?
	* Proponemos libros
		* A Gentle Introduction to symbolic computation
		* The Little Schemer
		* Mención hon(rr)orífica: SICP
	* Alguien se propone para dar el tema
	* Entre todæs ayudamos si hay dudas
* Promoción del seminario
