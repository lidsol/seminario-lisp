# Experiencias en LISP

#### `(FLISoL Ranchero 2021)`

---

### Anfitriones

* umoqnier (LIDSOL)
  * @umoqnier - TW
  * [lisdol](https://lidsol.org/authors/umoqnier/)
* cer0
  * GHB: [cer-0](https://github.com/cer-0)

---

### Agenda

* Paradigmas de programación
* Introducción LISP
* Dejar de pensar en forma estructurada
* Comunidad


---

## Paradigmas de programación

### ¿Qué son los paradigmas?

* Un paradigma es una forma en la cual se pueden abordar problemas para proponer una solución.

* Dependiendo del paradigma se tienen diversas técnicas y estrategias disponibles. Entonces, cierto conjunto de problemas serán más sencillos solucionarlos con ciertos paradigmas y otro conjunto de problemas con otros paradigmas.

* Los lenguajes de programación se clasifican en ciertos paradigmas por las características, modelo de ejecución, 

---

Un lenguaje puede ser clasificado con múltiples paradigmas

	* Python
	* Ruby
	* JavaScript
	* Perl

---

> Estos lenguajes son muy populares y es notable que encontraremos similitudes en aspectos básicos como condicionales, aritmética, colecciones, etc. Sin embargo, diferentes lenguajes son para diferentes cosas

---

### Las grandes clasificaciones de paradigmas

* **Imperativos:** Secuencia explicita de comandos que actualizan estados
* **Declarativos:** Programación por especificación del resultado que queremos, no "cómo" obtenerlo		
* **Orientados a objetos:** Todas las cosas, o la mayoría, son objetos. Abstracciones del mundo "real"

---

## Programación Estructurada (Imperativa)

* Un tipo de programación **imperativa**
* Definida por ciclos, condiciones y subrutinas (sustituyen los `gotos`)
* Ejecución secuencial del código
* Las variables van mutando con forme se avanza en el tiempo de ejecución

---

## C

```c
#include<stdio.h>

void main(){
    int total = 0;
    int i;
    for(i = 1; i <= 10; i++){
        total = total + i;
    }
    printf("Total: %d\n", total);
}
```

---

### Programación Orientada a Objetos

* Los objetos encapsulan estados (atributos, variables, campos) y comportamientos (metodos).
* Los objetos modifican sus propios estados internos `self`
* Interacción y comunicación atraves de mensajes
	* `un_arreglo.lenght();  // Calcula la longitud, un estado interno, y la devuelve`  
* Reusabilidad (Herencia, polimorfismo)

---

#### Ruby

```ruby
class CuentaBanco
	attr_reader :balance
	def initialize
		@balance = 0
	end
	def deposito cantidad
		@balance += cantidad
	end
	def retiro cantidad
		@balance -= cantidad
	end
end
```

---

```
> cuenta = cuentaBanco.new
#<CuentaBanco...>

> cuenta.balance
0

> cuenta.deposito 200
> cuenta.retiro 50

>cuenta.balance
150
```

---

## Programación Lógica (Declarativa)

* Se basa principalmente en la lógica formal


> Las cazuelas tienen orejas, Diego tiene orejas. Por tanto, Diego es una cazuela ;)

---

* Los programas no están basados en instrucciones si no en hecho y clausulas

* En lugar de describir "cómo" resolver un problema describimos el "qué" de la situación

* Elementos
	* `VARIABLE`
	* `constante`

---

### Prolog

```prolog
padre(homero, bart).
padre(homero, lisa).
madre(marge, bart).
madre(marge, lisa).
```

---
#### Ejemplos

```prolog
?- madre(X, bart). 
X = marge

?- madre(marge, Y).
Y = bart ? ;
Y = lisa
```

---

### Programación Funcional (Declarativa)

LISP `:P`

```lisp
(define (sum numeros)
  (if
    (null? numeros) 0
    (+ (car numeros) (sum (cdr numeros)))
  )
)
```
---

## Introducción a LISP

### Historia

*  Origenes:
  * En el inicio lenguajes de máquina (switches físicos)
  * Ensambladores (especificos del procesador y por tanto no portables)
  * Lenguajes compilados e intepretados
  
* John McCarthy [[1]] - 1959
  * Lenguaje de programación teórico
  * Minima sintaxis y semántica
  * Ejercicio intelectual de exploración matemática

[1]: http://www-formal.stanford.edu/jmc/recursive.pdf
---

###  Auge y caída

* Inteligencia Artificial
  * Su naturaleza matemática ayuda a su adopción
  * Académia
  * LISP Machines

*  El in(f)vierno de LISP ~ mid 1980
  * Inteligencia Artificial (la decepción)
  * Reducción de inversion en investigaciones
  * C++
    
---

###  El nuevo amanecer de LISP

* Los lenguajes populares toman ideas de LISP
  * _garbage colector_
  * Polimorfismo
* Un lenguaje poderoso
  * Muchas características _built-in_
  * Modificable
  * Interpretes de LISP en LISP en solo 50 líneas

---

### Características del lenguaje

---

#### Tipos de datos

- Números
  - `7` (enteros)
  - `3.1415925` (flotantes)
  - `3/4` (racionales)

---

#### Tipos de datos
- Símbolos
  - Son representaciones de datos (que pasan a ser representaciones de datos por sí mismas) :O
    - `LISP`
    - `FLISOL-2021`
    - `R2D2`
    - `T` (verdadero)
    - `NIL` (_nulo_ o falso)

---
#### Tipos de datos
- Listas
  - _planas_ o simples
    - `(EL FLISOL RANCHERO 2021)`
  - Listas _anidadas_
    - `(DESPUES (SIGUE (UN (TALLER))))`

---

#### Funciones

De dos tipos (hasta donde conocemos):

* Predicados
* Funciones comunes


---
#### Funciones

* Predicados
  * Funciones cuya única _respuesta_ (valor de retorno) son los símbolos `T` y `NIL`
    * Generalmente los predicados son seguidos de la letra `P`
      * `SYMBOLP` (_pregunta_ si el argumento es un símbolo)
      * `ODDP` (¿el argumento es impar?)
    * Claro, existen excepciones:
      * `NULL` (¿el argumento es una lista vacía `()`?)

---
#### Funciones

* Funciones comunes
  * Devuelven una expresión s (sexp)
    * `(quote null)`
    * `(car (quote (a b c)) ; => A`
    * `(cdr '(a b c)) ; => (B C)`

---
#### Condicionales

Juan me regañó por decir que `if` es la 

```lisp
(defun par-o-impar (x)
  (if (oddp x) 'impar
    'par))

(par-o-impar 55) ; => IMPAR
(par-o-impar 16) ; => PAR
```

---
#### Condicionales

```lisp
(defun signo (x)
  (cond
    ((> x 0) 'positivo)
    ((= x 0) 'neutro)
    (t 'negativo)))
```

---
## Dejar de pensar en forma estructurada

```lisp
(defun cuadrado (n)
  (* n n))
  
(cuadrado 3) ; => 9

(cuadrado (+ 2 2)) ; => 16
```
---
## Dejar de pensar en forma estructurada

```lisp
(defun suma-de-cua (n)
  (* n n))
  
(cuadrado 3) ; => 9

(cuadrado (+ 2 2)) ; => 16
```
---


## Comunidad

* ¿Como vamos aprendiendo? [[2]]
	1. Proponemos libros
		* A Gentle Introduction to Symbolic Computation
		* The Little Schemer
		* Mención hon(rr)orífica: SICP
	2. Alguien se propone para dar el tema
	3. Entre todæs ayudamos si hay dudas
* El seminario [[3]]
 
[2]: https://gitlab.com/lidsol/seminario-lisp/
[3]: https://lidsol.org/courses/lisp/
 


