# A Gentle Introduction to Symbolic Computation

Puedes encontrar el libro en [esta carpeta](../books).

Eventualmente podrás encontrar notas correspondientes a los capítulos del libro,
así como el código de los ejercicios propuestos.

Para poder ejecutar el código necesitarás alguna implementación de LISP,
nosotros usamos [Steel Bank Common Lisp][1], la cual está
disponible en distribuciones GNU/Linux como Debian y Arch Linux bajo el nombre
de paquete de `sbcl`.

## Soporte de editores

El libro The Common Lisp Cookbok contiene una [página][11] muy completa
acerca de los distintos editores y el soporte de estos para Common Lisp.


## Evaluando código en SBCL

Estas son instrucciones a seguir para ejecutar código en el REPL de [SBCL][1]
desde algunos editores de texto populares, esto con el propósito de ahorrarnos
los pasos de copiar código de LISP y pegarlo en el REPL cada que necesitemos
evaluar expresiones.

### EMACS

#### Doom EMACS

[Doom Emacs][5] es capaz de configurar [Sly][8], un fork de [SBCL][1], para evaluar código
de Lisp directamente desde un archivo. Puedes encontrar más
información acerca en su [documentación][9].

Para usar [Doom Emacs][5] será necesario contar con el editor y haber 
respaldado previamente tus archivos de configuración (`~/.emacs.d`).

Puedes encontrar las instrucciones de instalación en [este enlace][6].

Una vez instalado necesitarás habilitar el módulo de Lisp (Para más detalles
consulta [la documentación][7]):

1. edita el archivo de configuración de [Doom][5] (`$DOOMDIR/init.el`)
2. ubica a la sección `:lang `y descomenta el módulo de `common-lisp`. 
3. ejecuta en una terminal: `doom sync`

> `doom` se encuentra en `$DOOMDIR`, usualmente `~/.doom.d/` o `~/.config/doom.d/`

Algunos atajos que deberías memorizar (obtenidos de la [documentación][10]):

* `M-C-x`: Evalúa toda una s-expresión, hasta el nivel superior de ésta. Muestra
  el resultado en el área _echo_ (ubicada en la parte inferior).
* `C-x C-e`: Evalúa la s-expresión actual. Muestra el resultado en el área echo.
* `C-c :`: Realiza una evaluación interactiva en el área echo.
* `C-c C-r`: Evalúa la región.

> Si precedemos a `M-C-x` o `C-x C-e` de un número se insertará el resultado
> de la evaluación en el buffer actual, en lugar de mostrarlo en el área echo.

Para presentar la salida en un nuevo buffer (ventana) podemos presionar `Spc m '`
o ejecutar `M-x sly`, seguido deberemos seleccionar la instancia de common Lisp
a ejecutar (SBCL). Al momento de evaluar con `M-C-x` el resultado se mostrará
en el nuevo buffer.

### (Neo)Vim

Son escasas las soluciones para hacer de Vim un "ambiente" de
creación de Lisp. Superior Lisp Interaction Mode for Vim (slimv)
es una de las más robustas.

Para poder hacer uso de slimv necesitamos instalar un multiplexador
de terminal y un administrador de plugins de vim. Usaremos [tmux][2]
y [`vim-plug`](https://github.com/junegunn/vim-plug) respectivamente.

Una vez instalados los requisitos podremos agregar los siguientes
repositorios a tu archivo de configuración de Vim 
(`~/.vimrc` o `~/.config/nvim/init.vim` según el caso):

* [slimv][3]: Superior Lisp Interaction Mode for Vim
* [rainbow_parentheses][4]: colored parentheses

``` vim-script
call plug#begin()
Plug 'junegunn/rainbow_parentheses.vim'
Plug 'kovisoft/slimv'
call plug#end()
```

Descargamos e instalamos los plugins ejecutando `:PlugInstall` y agregamos
las siguientes líneas al final de nuestro archivo de configuración de Vim:

``` vim-script
" slimv
let g:slimv_repl_split = 2 " muestra el REPL debajo del buffer principal

" habilita rainbow parenthesis en archivos de lisp y scheme
augroup rainbow_lisp
        autocmd!
        autocmd FileType lisp,scheme RainbowParentheses
augroup END
```

Ahora desde una terminal ejecutando [`tmux`][2] podrás usar
Vim y evaluar código Lips en el REPL de [SBCL][1].

Algunos atajos que deberías memorizar:

``` vim-snippet
,c    " Conectar al servidor
,d    " evaluar definición de función (defun)
,e    " evaluar la s-expresión actual
,r    " evaluar la región seleccionada
,b    " evaluar el buffer (contenido de la ventana)
```

> Si un registro precede a los comandos `,d ,e ,r`
> entonces el resultado evaluación es guardado en ese mismo,
> ej: `"i,d` almacena la evaluación de la función en el registro `"i`

Revisa la documentación de [`slimv`][3] para más información, puedes
encontrar más atajos de teclado consultando `:h slimv-keyboard`.

[1]:http://www.sbcl.org/
[2]:https://github.com/tmux/tmux
[3]:https://github.com/kovisoft/slimv
[4]:https://github.com/junegunn/rainbow_parentheses.vim
[5]:https://github.com/hlissner/doom-emacs
[6]:https://github.com/hlissner/doom-emacs#install
[7]:https://github.com/hlissner/doom-emacs/blob/develop/docs/getting_started.org#modules
[8]:https://github.com/joaotavora/sly/
[9]:https://joaotavora.github.io/sly/
[10]:https://joaotavora.github.io/sly/#Working-with-source-files
[11]:https://lispcookbook.github.io/cl-cookbook/editor-support.html
