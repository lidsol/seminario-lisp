(defun hacer-tablero ()
  (list 'tablero 0 0 0 0 0 0 0 0 0))

(defun convierte-a-letra (letra)
    (cond ((equal letra 1) "O")
          ((equal letra 10) "X")
          (t " ")))

(defun imprime-fila (x y z)
  (format t "~&   ~A | ~A | ~A"
          (convierte-a-letra x)
          (convierte-a-letra y)
          (convierte-a-letra z)))

(defun imprime-tablero (tablero)
  (format t "~%")
  (imprime-fila (nth 1 tablero) (nth 2 tablero) (nth 3 tablero))
  (format t "~%  -----------")
  (imprime-fila (nth 4 tablero) (nth 5 tablero) (nth 6 tablero))
  (format t "~%  -----------")
  (imprime-fila (nth 7 tablero) (nth 8 tablero) (nth 9 tablero))
  (format t "~%"))

(defun hacer-jugada (jugadr pos tablero)
  (setf (nth pos tablero) jugadr)
  tablero)

(defvar *tripletes* '((1 2 3) (4 5 6) (7 8 9)
                     (1 4 7) (2 5 8) (3 6 9)
                     (1 5 9) (7 5 3)))

(defun sumar-triplete (tablero triplete)
  (+ (nth (first  triplete) tablero)
     (nth (second triplete) tablero)
     (nth (third  triplete) tablero)))

(defun computar-sumas (tablero)
  (mapcar #'(lambda (triplete) (sumar-triplete tablero triplete))
          *tripletes*))

(defvar *computadora* 10)
(defvar *oponente* 1)

(defun ganador-p (tablero)
  (let ((sumas (computar-sumas tablero)))
    (or (member (* *computadora* 3) sumas)
        (member (* *oponente* 3) sumas))))

(defun tiro-de-oponente (tablero)
	(let* ((pos (leer-jugada-legal tablero))
	       (nuevo-tablero (hacer-jugada *oponente* pos tablero)))
	  (imprime-tablero nuevo-tablero)
	  (cond ((ganador-p nuevo-tablero) (format t "~&Ganaste (˵ ͡° ͜ʖ ͡°˵)"))
	        ((tablero-lleno-p nuevo-tablero) (format t "~&Empate ⤜(ⱺ ʖ̯ⱺ)⤏"))
	        (t (tiro-de-computadora nuevo-tablero)))))

(defun leer-jugada-legal (tablero)
  (let ((pos (read (format t "~&Jugada: "))))
    (cond ((not (and (integerp pos) (<= 1 pos 9)))
           (format t "~&Entrada invalida ಠ_ಠ")
           (leer-jugada-legal tablero))
          ((not (zerop (nth pos tablero)))
           (format t "~&Ese lugar ya esta ocupado O=('-'Q)")
           (leer-jugada-legal tablero))
          (t  pos))))

(defun jugar-una-partida ()
	(if (y-or-n-p "¿Tiras primero?")
		(tiro-de-oponente (hacer-tablero))
		(tiro-de-computadora (hacer-tablero))))

(defun tablero-lleno-p (tablero)
  (not (member 0 tablero)))

(defun elegir-mejor-jugada (tablero)
  (or (tres-seguidas tablero)
      (bloquear-victoria-oponente tablero)
	  (bloquear-jugada-de-apachurre tablero)
	  (bloquear-jugada-dos-uno tablero)
      (jugada-aleatoria tablero)))

(defun tiro-de-computadora (tablero)
  (let* ( (mejor-jugada (elegir-mejor-jugada tablero))
          (pos (first mejor-jugada))
          (estrategia (second mejor-jugada))
          (nuevo-tablero (hacer-jugada *computadora* pos tablero)))
    (format t "~&Mi jugada: ~S" pos)
    (format t "~&Mi estrategia: ~A~% " estrategia)
    (imprime-tablero nuevo-tablero)
    (cond ((ganador-p nuevo-tablero) (format t "~&¡Gané!"))
          ((tablero-lleno-p nuevo-tablero) (format t "~&¡Empate!"))
          (t (tiro-de-oponente nuevo-tablero)))))

(defun jugada-aleatoria (tablero)
  (list (elegir-posición-vacía-aleatoria tablero) "Jugada Aleatoria"))

(defun elegir-posición-vacía-aleatoria (tablero)
  (let ((pos (1+ (random 9))))
    (if (zerop (nth pos tablero))
      pos
      (elegir-posición-vacía (tablero)))))

(defun encontrar-posición-vacía  (tablero cuadros)
(find-if #'(lambda (pos) (zerop (nth pos tablero))) cuadros))

(defun tres-seguidas (tablero)
  (let ((pos (ganar-o-bloquear tablero (* 2 *computadora*))))
    (and pos (list pos "hacer tres seguidas"))))

(defun ganar-o-bloquear (tablero suma-objetivo)
  (let ((triplete
            (find-if #'(lambda (trip)
                (equal (sumar-triplete tablero trip) suma-objetivo)) *tripletes*)))
        (when triplete (encontrar-posición-vacía tablero triplete))))

(defun bloquear-victoria-oponente (tablero)
  (let ((pos (ganar-o-bloquear tablero (* 2 *oponente*))))
    (and pos (list pos "bloquear oponente"))))

(defvar *esquinas* '(1 3 7 9))

(defvar *lados* '(2 4 6 8))

(defun bloquear-jugada-de-apachurre (tablero)
  (cond ((or
		  (and
		   (equal (nth 1 tablero) 1)
		   (equal (nth 9 tablero) 1))
		  (and
		   (equal (nth 3 tablero) 1)
		   (equal (nth 7 tablero) 1)))
		 (list (encontrar-posición-vacía tablero *lados*) "bloquear jugada de apachurre"))
		(t nil)))

(defun bloquear-jugada-dos-uno (tablero)
  (cond ((and
		  (equal (nth 5 tablero) 1)
		  (mapcar #'(lambda (esquina) (equal (nth esquina tablero) 1)) *esquinas*))
		 (list (encontrar-posición-vacía tablero *esquinas*) "bloquear jugada de dos sobre uno"))
		(t nil)))
