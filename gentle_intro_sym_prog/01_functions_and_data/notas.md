# Funciones y datos

En este capítulo se habla de datos, específicamente de _números_ y _símbolos_. Se muestran algunas funciones simples para operar ambos.
Los predicados son funciones especiales que devuelven los booleanos `T` (verdadero) o `NIL` (falso/vacío).

## Tipos de números

```lisp
256  ; Entero (int)
3.14 ; Flotante (float)
1/2  ; Racional (ratio)
```

## S-expressions

Lisp surge en el contexto del programa de investigación de la inteligencia artificial simbólica, como consecuencia todo en Lisp es una *expresión simbólica*, o _S-expression_. Las S-expressions pueden ser átomos o listas[^1]. 

```lisp
(car '(a b c)) ; representa una expresión simbólica
'lisp ; representa una expresión simbólica
```

[^1]: En rigor, átomos o estructuras cons, es decir, expresiones simbólicas "sueltas" y expresiones simbólicas que inician y terminan con paréntesis. Las listas son las estructuras cons más comunes, aunque existen estructuras cons que no son listas. El capítulo 2 aborda más sobre el tema.

## Símbolos
Son un tipo de dato en Lisp, se les nombra con letras, combinaciones de éstas con guiones o números y algunos otros caracteres especiales. Algunos símbolos (como `pi`) tienen valores ya definidos en Lisp; otros no y debemos asignarles valores antes de evaluarlos.

Ejemplos:

```lisp
1isp
24-7
car
R2D2
+
```

`T` y `NIL` Son símbolos especiales, significan _verdadero_ y _falso_ respectivamente.

Técnicamente, cualquier símbolo distinto a `nil` (non-nil) se evalúa como verdadero, aunque con la excepción de `T`, tienen un valor adicional, por ejemplo, `2` se evalúa como verdadero y como el número `2`. Sin embargo, es importante considerar que `nil` tiene expresiones simbólicas equivalentes como `()`, `'()`, `nil` y `'nil`.

## Predicados

Son funciones cuya única posible respuesta son los símbolos `T` o `NIL`.

```lisp
numberp ; devuelve T si su entrada es un número, NIL en otro caso.
oddp    ; devuelve T si su entrada es un número par, NIL en otro caso.
```

Generalmente la `p` al final de la función significa predicado, existen excepciones, por ejemplo:

```lisp
>       ; devuelve T si la primer entrada es mayor que la segunda.
equal   ; devuelve T si la primer entrada es la misma que la segunda.
null    ; devuelve T si la entrada es una lista vacía (un NIL).
```
