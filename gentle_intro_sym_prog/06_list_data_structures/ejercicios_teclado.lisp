;;; Ejercicio 6.41

(defvar *cuartos*) ; Variable global que almacena en cada entrada un cuarto

(defvar *ubicacion-actual*) ; Ubicación del robot Robbie

(setf *ubicacion-actual* 'alacena) ; Robbie inicia su recorrido en la alacena

(setf
  *cuartos* '((la-biblioteca '((este cuarto-de-arriba)
                         (sur escaleras-de-detras)))
              (escaleras-de-detras '((norte la-biblioteca)
                             (sur cuarto-de-abajo)))
              (cuarto-de-abajo '((norte escaleras-de-detras)
                                    (este el-comedor)))
              (cuarto-de-arriba '((oeste la-biblioteca)
                                  (sur escaleras-de-delante)))
              (escaleras-de-delante '((norte cuarto-de-arriba)
                              (sur sala)))
              (sala '((norte escaleras-de-delante)
                             (este la-cocina)
                             (sur el-comedor)))
              (la-cocina '((oeste sala)
                         (sur alacena)))
              (alacena '((norte la-cocina)
                        (oeste el-comedor)))
              (el-comedor '((norte sala)
                             (este alacena)
                             (oeste cuarto-de-abajo)))))
;;; Aqui era posible asignar cada entrada de la forma (cuarto (d1 c1))
;;; De esta forma los artilugios en elecciones no sería necesarios.

(defun elecciones (cuarto) ; inciso A
  ;;; muestra las posibles direcciones y sus cuartos correspondientes desde
  ;;; un cuarto en específico.
  (eval (cadr (assoc cuarto *cuartos*))))

(defun buscar (direccion cuarto) ; inciso B
  ;;; devuelve el nombre del cuarto que hallaremos en la variable dirección
  ;;; estando en el cuarto que se recibió como segundo argumento.
  (second (assoc direccion (elecciones cuarto))))

(defun actualizar-ubicacion-de-robbie (cuarto) ; inciso C
  ;;; actualiza la ubicación de Robbie a un cuarto en específico.
  (setf *ubicacion-actual* cuarto)
  (donde))

(defun numero-de-elecciones () ; inciso D
  ;;; devuelve a cuántos posibles cuartos nos podemos mover basados en la
  ;;; ubicación de Robbie.
  (length (elecciones *ubicacion-actual*)))

(defun arriba-p (cuarto) ; inciso E
  ;;; predicado que pregunta si el cuarto está arriba de las escaleras.
  (or (equal cuarto 'la-biblioteca)
      (equal cuarto 'cuarto-de-arriba)))

(defun en-escaleras-p (cuarto) ; inciso E
  ;;; predicado que pregunta si el cuarto está en escaleras (traseras o frontales).
  (or (equal cuarto 'escaleras-de-delante)
      (equal cuarto 'escaleras-de-detras)))

(defun donde () ; inciso F
  ;;; devuelve una lista que muestra la ubicación de Robbie.
  (cond
    ((arriba-p *ubicacion-actual*) `(robbie esta arriba en ,*ubicacion-actual*))
    ((en-escaleras-p *ubicacion-actual*) `(robbie esta en ,*ubicacion-actual*))
    (t `(robbie esta abajo en ,*ubicacion-actual*))))

(defun mover (direccion) ; inciso G
  ;;; mueve a Robbie de un cuarto a otro basado en su ubicación actual.
  (if (null (buscar direccion *ubicacion-actual*))
      '(¡Ups! ¡Robbie chocó con una pared!)
      (actualizar-ubicacion-de-robbie (buscar direccion *ubicacion-actual*))))

;;; Llevando a Robbie a dar un paseo 

(donde)
(elecciones *ubicacion-actual*)
(buscar 'norte *ubicacion-actual*)
(mover 'norte)
(mover 'norte) ; chocar con pared
(mover 'oeste)
(mover 'norte)
(mover 'norte)
(mover 'oeste)
