# Listas como estructuras de datos

No por algo el lenguaje se llama LISt Processing. El capítulo muestra como usar listas para representar otros tipos de datos, como conjuntos (sets) o tablas (association lists), así como algunas funciones para manipularlos.

## La función `append`

`append` nos permite tomar dos listas como entrada y devuelve una lista que contiene todos los elementos de la primera lista seguida de la segunda.

```lisp
(append '(1 2) '(3 4)) ; => (1 2 3 4)
(append '(x y) 'z)     ; => (x y . z)
```

## Más funciones para listas

`reverse` recibe una lista y la devuelve invertida, la última s-expresión se vuelve la primera.

```lisp
(reverse '(a b (c d))) ; => ((c d) b a)
```

`nth` recibe una lista y devuelve la enésima s-expresión, similarmente `nthcdr` regresa el resto de la lista a partir de la enésima s-expresión.

```lisp
(nth    1 '(a b (c d))) ; => b
(nthcdr 1 '(a b (c d))) ; => (b (c d))
```

`last` recibe una lista y devuelve la última s-expresión de ella en una lista.

```lisp
(last '(limon fruta (pera mango))) ; => ((pera mango))
(last '(1 2 3)) ; => (3)
```

`remove` elimina todas las coincidencias de un elemento en una lista.

```lisp
(remove 'a '(1 a 2 3 a 4)) ; => (1 2 3 4)
```

## Listas como conjuntos

Las operaciones que tratan a las listas como conjuntos no preservan el orden de las listas originales y eliminan los elementos repetidos.

`member` Verifica si un elemento es miembro de una lista. Si no lo es, regresa nil. Si sí, regresa una lista que comienza con el valor que se pasó como argumento. Ya que esta lista es un símbolo non-nil, puede contar como `T` en ciertos contextos (es como un `T` con información adicional), pero es posible que tengamos pérdida o exceso de información, pues no es una función proposicional.

```lisp
(member 'y '(x y z)) ; => (y z)
(member 'a '(1 2 3)) ; => nil
```

`intersection` Devuelve los elementos en común de dos listas.

```lisp
(intersection '(c b a) '(e d c)) ; => (c)
```

`union` Crea un _conjunto_ (lista) uniendo los elementos de dos listas (sin duplicados)

```lisp
(union '(b a) '(d c)) ; => (a b c d)
```

`set-difference` Devuelve una lista con los elementos de la primer lista que no estan en la segunda lista.

```lisp
(set-difference '(a b c) '(c d e)) ; => (a b)
```

`subsetp` devuelve un booleano (`t` o `nil`) si todos los elementos de la primera lista estan dentro de la segunda lista.

```lisp
(subsetp '(a b) '(d c b a)) ; => t
```

`length` Devuelve el numero de s-expresiones de una lista.

```lisp
(length '(a b (c d) e)) ; => 4
```

## Listas como tablas

También llamadas listas de asociación, dentro de ellas existen entradas y en cada una de estas tenemos el `car` de cada entrada, la cual es conocida como _llave_.

```lisp
(setf numeros '((one   uno) ; primera entrada
                (two   dos) ; segunda entrada
                (three tres)))
;;                ^     ^
;;              llave  valor
```

Funciones como `assoc` y `rassoc` nos permiten obtener la llave o el valor asignado a una llave, respectivamente, sin embargo `rassoc` solamente funciona si tenemos entradas como pares punteados (dotted-pairs).

```lisp
(assoc 'two numeros) ; => (two dos)

(rassoc 'dos numeros) ; => nil
(rassoc 'dos '((one . uno) (two . dos))) ; => (two . dos)
```
