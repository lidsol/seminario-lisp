;; inciso a
(defun sobre-espacio (n)
  "Muestra en pantalla 'N' espacios"
  (cond
    ((< n 0) (format t "~&Error!"))
    ((equal n 0) (format t ""))
    (t (format t " ") (sobre-espacio (1- n)))))

(defun prueba (n)
  "Ayuda a visualizar la salida de 'SOBRE-ESPACIO'"
  (format t "~%>>>")
  (sobre-espacio n)
  (format t "<<<"))

;; inciso b
(defun grafica-punto (cadena-a-graficar valor-y)
  "Imprime 'cadena-a-graficar' en la columna 'valor-y'"
  (sobre-espacio valor-y)
  (format t "~A~%" cadena-a-graficar))
(grafica-punto "#" 10)

;; inciso c
(defun grafica-puntos (cadena-a-graficar lista-puntos)
  "Ejecuta 'grafica-punto' sobre una lista de puntos"
  (mapcar #'(lambda (punto) (grafica-punto cadena-a-graficar punto))
          lista-puntos))

;; inciso d
(defun genera (m n)
  "Devuelve una lista de los números enteros que hay entre m y n"
  (cond
    ((eq m n) (cons n '()))
    (t (cons m (genera (1+ m) n)))))

;; inciso e
(defun grafica ()
  "Pide los valores de func, start, end y plotting-string y grafica la función."
  (let* ((función (read (format t "~%¿Cuál es la función? ")))
         (inicio  (read (format t "~%¿Cuál es el valor de inicio? ")))
         (cierre  (read (format t "~%¿Cuál es el valor de cierre? ")))
         (cadena-a-graficar (read (format t "~%¿Cuál es el valor de la cadena a graficar? "))))
    (grafica-puntos cadena-a-graficar (mapcar función (genera inicio cierre)))))

;; inciso f
(defun cuadrado (x)
  "Devuelve el cuadrado del 'X'"
  (* x x))

(grafica)
