#+Title: I/O

* Introducción

Las funciones de I/O (Input/Output) presentadas en el capítulo conforman un núcleo pequeño y estandarizado. Más allá de dicho núcleo, el manejo de I/O depende mucho de la implementación de Common Lisp.

Permiten interactuar con lo que se imprime en el display, así como leer y escribir desde y a archivos en disco.


* La función ~format~

Función principal para imprimir en el display o en un archivo la cadena especificada en el argumento.

El primer argumento es, para el caso de impresión en display, ~t~, y para el caso de impresión en un archivo, una cadena con su ubicación.

** t

#+begin_src lisp
  (format t "Hola, mundo.")
#+end_src

#+RESULTS:
: NIL


** stream en vez de t

#+begin_src lisp
  (format "/dirección/del/archivo" "Hola, mundo.")
#+end_src

Es posible añadir parámetros a esta función para modificar la cadena:


** ~~S~

Se sustituye por un símbolo especificado al final de la cadena. Escribir en archivo con este parámetro permite leer de regreso lo que se escriba como un símbolo de Lisp.

 #+begin_src lisp
   (format t "El valor de pi es ~S" pi)
 #+end_src

 #+RESULTS:
 : NIL
 
 
** ~~D~

Se sustituye por un número entero en notación decimal.

#+begin_src lisp
  (format t "Lisp surge en los años ~D" (* 5 10))
#+end_src

#+RESULTS:
: NIL


** ~~F~

Se sustituye por un número flotante en notación decimal.

#+begin_src lisp
  (format t "El ~F está entre el 1 y el 2" (/ 3 2))
#+end_src


** ~~%~

Se sustituye por un salto de línea. Dos seguidos dejan una línea en blanco.

#+begin_src lisp
  (format t "Hola, mundo.~%Quiubo~%~%")
#+end_src


** ~~&~

Sólo da un salto de línea si no hubo uno antes. En caso contrario, no tiene ningún efecto.

#+begin_src lisp
  (format t "Hola, mundo.~&~&")
#+end_src


** ~~A~

Similar a ~~S~, pero sin caracteres escapados. 

#+begin_src lisp
  (format t "El valor de pi es ~A" pi)
#+end_src


* Algunas otras funciones básicas

** ~stringp~

Función proposicional, toma un objeto como argumento y evalúa si es o no del tipo string.

#+begin_src lisp
(stringp 1)
#+end_src

#+RESULTS:
: NIL

#+begin_src lisp
(stringp "1")
#+end_src

#+RESULTS:
: T


** ~read~

Lee un objeto del teclado y lo devuelve sin evaluarlo.

#+begin_src lisp
    (defun cuadrado-read ()
      "Toma un número como entrada del teclado del usuario para calcular su cuadrado"
      (format t "Inserte un número: ")
      (let ((x (read)))
        (format t "El cuadrado del número ~S es ~S. ~%" x (* x x))))
#+end_src

Así, ~read~ guarda el valor en una variable local, que se usa en la función, en este caso, para calcular su cuadrado.

#+begin_src lisp
  (cuadrado-read)
#+end_src


** ~yes-or-no-p~

Función proposicional que toma como argumento una cadena, que sirve como pregunta para el usuario, y evalúa si el usuario respondió con "yes" o con "no".

#+begin_src lisp
  (defun acertijo ()
    "Pregunta al usuario si busca o no la iluminación Zen."
    (if (yes-or-no-p
         "¿Buscas la iluminación Zen?")
        (format t "¡Entonces no la pidas!")
        (format t "La has encontrado.")))
#+end_src

#+begin_src lisp
(acertijo)
#+end_src

Una versión equivalente pero más corta es ~y-or-n-p~, que sólo evalúa "y" o "n".

#+begin_src lisp
  (defun acertijo-2 ()
    "Pregunta al usuario si busca o no la iluminación Zen."
    (if (y-or-n-p
         "¿Buscas la iluminación Zen?")
        (format t "¡Entonces no la pidas!")
        (format t "La has encontrado.")))
#+end_src

#+begin_src lisp
(acertijo-2)
#+end_src


** ~with-open-file~

Esta función permite usar como input el contenido de un archivo especificado en el argumento, separado por objetos.

El contenido se guarda en una variable local, como si se usara ~let~, y opcionalmente se manipula en el cuerpo de la función.

Supongamos que tenemos un archivo 

#+begin_src lisp

#+end_src

~:direction :output~

#+begin_src lisp

#+end_src


* Funciones de impresión

Definidas en Lisp 1.5, se puede obtener lo mismo con ~format~. Definamos una variable con una cadena para hacer pruebas:

#+begin_src lisp
  (defvar *prueba* "¡Hola, esto es una prueba!")
#+end_src

#+RESULTS:
: *PRUEBA*

** ~terpri~

"Terminate print", sirve para indicar que termina una impresión y dar un salto de línea.


** ~prin1~ (~~S~)

Arroja una cadena.

#+begin_src lisp
  (prin1 *prueba*)
#+end_src


** ~princ~ (~~A~)

Arroja una cadena sin caracteres escapados.

#+begin_src lisp
  (princ *prueba*)
#+end_src


** ~print~

Una combinación de las tres, arroja una línea nueva con ~terpri~, imprime la cadena con ~prin1~ y luego un espacio con ~princ~.

#+begin_src lisp

#+end_src


** ~dribble~

Sirve para registrar una sesión de Common Lisp en un archivo indicado.

#+begin_src lisp
  (dribble "session.log")
#+end_src


** ~'$eof$~

A veces, al leer el contenido de un archivo, Lisp puede arrojar un error cuando termina el documento. Para evitar esto, se añade como argumento una celda cons nueva, indicada aquí como eof (end-of-line).

#+begin_src lisp

#+end_src


