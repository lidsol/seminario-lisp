;; 9.1
(defun pilots ()
  (format t "~&There are old pilots, and there are bold pilots, but there are no old bold pilots."))

;; 9.2
(defun imprime-caracter (caracter)
  "Muestra en pantalla 'CARACTER'"
  (format t "~A" caracter))

(defun imprime-espacio ()
  "Muestra un espacion en blanco ' '"
  (format t " "))

(defun dibuja-linea (longitud)
  "Muestra una línea de caracteres '#' de tamaño 'LONGITUD'"
  (cond ((eq longitud 1) (imprime-caracter "#") (format t " ~%"))
        (t (imprime-caracter "#") (dibuja-linea (1- longitud)))))

(defun dibuja-extremo-d (longitud)
  "Imprime el caracter '#' seguido de 'LONGITUD'-1 espacios en blanco"
  (cond ((eq longitud 1) (imprime-caracter "#") (format t "~%"))
        (t (imprime-espacio) (dibuja-extremo-d (1- longitud)))))

(defun dibuja-extremos (longitud)
  "Dibuja una linea donde el primer y último caracter son '#',
  los demás caracteres son espacios"
  (imprime-caracter "#")
  (dibuja-extremo-d longitud))

;; 9.3
(defun dibuja-caja-rellena (base altura)
  "Dibuja una caja rellena de caracteres '#' de base 'BASE' y
  altura 'ALTURA'"
  (cond ((eq altura 1) (dibuja-linea base))
        (t (dibuja-linea base)
           (dibuja-caja-rellena base (1- altura)))))

(defun dibuja-caja-hueca (base altura is-tapa)
  "Imprime una caja de espacios vacíos con contorno de caracteres
  '#' de base 'BASE' y altura 'ALTURA'. IS-TAPA es un parametro que
  indica cuándo imprimir la tapa de la caja."
  (cond ((eq altura 1) (dibuja-linea base))
        ((eq is-tapa t) (dibuja-linea base)
                        (dibuja-caja-hueca base (1- altura) (not is-tapa)))
        (t (dibuja-extremos base)
           (dibuja-caja-hueca base (1- altura) is-tapa))))

;; 9.5
(defun valida-caracter-gato (caracter)
  "Devuelve un espacio si el caracter que recibe es NIL"
  (if (null caracter) " " caracter))

(defun dibuja-fila-gato (lista)
  "Imprime en pantalla un fila del tablero de gato"
  (format t "~&~A | ~A | ~A~%"
          (valida-caracter-gato (first lista))
          (valida-caracter-gato (second lista))
          (valida-caracter-gato (third lista))))

(defun dibuja-tablero-gato (lista)
  "Muestra el tablero del juego gato usando 'LISTA'
  como las tiradas realizadas"
  (cond
    ((null lista) (imprime-espacio))
    ((eq (length lista) 3) (dibuja-fila-gato lista))
    (t (dibuja-fila-gato lista)
       (dibuja-linea 9)
       (dibuja-tablero-gato (nthcdr 3 lista)))))

(dibuja-tablero-gato '(X NIL X O X NIL O O X))
