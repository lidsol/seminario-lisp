;;13.1
(defun subprop (simbolo valor propiedad)
  "Elimina una valor de la propiedad de un símbolo"
  (setf (get simbolo propiedad)
        (remove valor (get simbolo propiedad))))

;; 13.2
(defun forget-meeting (persona1 persona2)
  "Elimina de la propiedad 'conocidos' a dos personas entre sí"
  (subprop persona1 persona2 'conocidos)
  (subprop persona2 persona1 'conocidos))

(setf (get 'david 'conocidos) '(paul mccarthy))
(setf (get 'paul 'conocidos) '(mccarthy david))

(forget-meeting 'paul 'david)

(get 'paul 'conocidos)
(get 'david 'conocidos)

;13.3
(defun nuestro-get (x y)
    (cadr (member y (symbol-plist x))))



;13.4
(defun hasprop (x y)
    (cond ((member y (symbol-plist x)) t)))


;13.5 Da una ventaja de los arreglos por encima de las listas.
Los arreglos operan con mayor rapidez que las listas.

;13.6 Da una ventaja de las listas por encima de los arreglos.
Son más fáciles de declarar. Son celdas cons.

#|13.7 Cuál requiere más celdas cons: una lista de asociación o una lista de
asociación con pares puntuados. |#
;->(valor); -> (resto)
La lista con dotted pair
