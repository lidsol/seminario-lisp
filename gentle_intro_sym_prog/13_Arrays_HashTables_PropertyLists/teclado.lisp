; Hacer un programa que genere un histograma.


(defvar *arreglo-hist*)
(defvar *puntos-totales*)

(defun nuevo-histo (bins)
    (setf *puntos-totales* 0)
    (setf *arreglo-hist* (make-array bins :initial-element 0)))

(defun guardar-valor (n)
  (cond ((and (<= n 10) (>= n 0))
		 (progn
		   (incf (aref *arreglo-hist* n))
		   (incf *puntos-totales*)))
		(t (format t "El valor no es válido."))))

(defun imprime-l-hist (n)
  (let ((valor (aref *arreglo-hist* n)))	
	(format t "~&~2D [~3D] " n valor)
			(dotimes (i valor)
			  (format t "*"))))

(defun imprime-histograma ()
  (dotimes (i 11)
	(imprime-l-hist i)))
