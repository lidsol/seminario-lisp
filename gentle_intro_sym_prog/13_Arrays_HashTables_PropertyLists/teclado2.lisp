;; Criptograma a decodificar
;; zj ze kljjls jf slapzi ezvlij pib kl jufwxuj p hffv jupi jf enlpo pib slafml pvv bfwkj

(defvar criptograma '("zj ze kljjls jf slapzi ezvlij pib kl jufwxuj p hffv jupi jf" "enlpo pib slafml pvv bfwkj"))

(defvar *tabla-cifrado* (make-hash-table )) ;Tabla de valores decifrados
(defvar *tabla-decifrado* (make-hash-table));Tabla de valores correspondientes

(defun hacer-reemplazo (x y) ;"x" es la variable cifrada "y" el valor real
    (setf (gethash y *tabla-cifrado*) x)
    (setf (gethash x *tabla-decifrado*) y))

(defun deshacer-reemplazo (x y)
  (setf (gethash y *tabla-cifrado*) nil)
  (setf (gethash x *tabla-decifrado*) nil))

(defun limpiar ()
  "Limpia las entradas de una tabla hash"
  (clrhash *tabla-cifrado*)
  (clrhash *tabla-decifrado*))

(defun decifra-cadena (cadena-cifrada)
  (do* ((longitud (length cadena-cifrada))
		(nueva-cadena (make-string longitud
								   :initial-element #\Space))
		(i 0 (1+ i)))
	   ((equal i longitud) nueva-cadena)
	(let* ((char (aref cadena-cifrada i))
		   (nuevo-char (gethash char *tabla-decifrado*)))
	  (when nuevo-char
		(setf (aref nueva-cadena i) nuevo-char)))))

(defun muestra-linea (linea)
  (format t "~&~A" linea)
  (format t "~&~A" (decifra-cadena linea)))

(defun muestra-texto ()
  (format t "~&---------")
  (muestra-linea (car criptograma))
  (format t "~%~%")
  (muestra-linea (cadr criptograma))
  (format t "~&---------"))

(defun recibe-primer-caracter (x)
  (char-downcase
   (char (format nil "~A" x) 0)))

(defun recibe-letra ()
  (let ((letra (read)))
    (cond
      ((or (equal letra 'end)
           (equal letra 'undo)) letra)
      (t (recibe-primer-caracter letra)))))

(defun sustituye-letra (letra)
  (when (gethash letra *tabla-decifrado*)
	(format t "La letra ~A ya ha sido decifrada como ~A." letra (gethash letra *tabla-decifrado*))
	(return-from sustituye-letra nil))
	(format t "¿Cómo se decifra esta letra?:~%")
	(let
		((nueva (recibe-letra)))
	  (cond ((not (characterp nueva))
			 (format t "~%Respuesta inválida"))
			((gethash nueva *tabla-cifrado*)
			 (format t "~%Pero '~A' ya ha sido decifrada como '~A'!"
					 (gethash nueva *tabla-cifrado*) nueva))
			(t (hacer-reemplazo letra nueva )))))

(defun deshaz-letra ()
  (format t "~&¿Deshacer cuál letra?~%")
  (let* ((x (recibe-letra))
		 (y (gethash x *tabla-decifrado*)))
	(cond ((not (characterp x))
		   (format t "~%Respuesta inválida~%"))
		  (y (deshacer-reemplazo x y))
		  (t (format t "~%Pero '~A' no ha sido decifrada~%" x)))))

  
(defun resuelve ()
  (do ((resp nil))
	  ((equal resp 'end))
	(muestra-texto)
	(format t "~%¿Substituye cuál letra?~%")
	(setf resp (recibe-letra))
	(cond ((characterp resp) (sustituye-letra resp))
		  ((equal resp 'undo) (deshaz-letra))
		  ((equal resp 'end) nil)
		  (t (format t "~%Respuesta inválida~%"))))) 
