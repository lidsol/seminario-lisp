# Listas

Este capítulo introduce al tipo de datos más versátil en Lisp: *listas*. Pueden
contener números, símbolos, u otras listas como elementos.

Podemos _separar_ listas con `CAR` y `CDR` y _juntarlas_ usando `CONS`,
`APPEND` o `LIST`. La función `LENGTH` cuenta el número de elementos de primer
nivel de una lista.

Puntos importantes sobre `CAR` y `CDR`:
- `CAR` y `CDR` solo aceptan listas como entrada.
- `FIRST` y `REST` funcionan igual que `CAR` y `CDR`.
- `SECOND` y `THIRD` funcionan igual que `CADR` y `CADDR`.
- Common Lisp incorpora funciones `C...R` para todas las combinaciones de `CAR`
y `CDR` con hasta cuatro `A`s y `D`s.

Propiedades de `NIL`:
- `NIL` es un símbolo. Es la única forma de expresar "no" o "falso" en Lisp.
- `NIL` es una lista vacía; su (longitud) valor `LENGTH` es cero.
- `NIL` es el único objeto de Lisp que es tanto un símbolo como una lista.
- `NIL` marca el final de una cadena de celdas cons. Cuando la listas son
impresas en notación de paréntesis, los `NIL`s finales se omiten por
convención.
- `NIL` y () son notaciones intercambiables.
- El `CAR` y `CDR` de `NIL` son a su vez `NIL`.

## Aspecto de las Listas

*Representación impresa*: La más conveniente de usar, pues es compacta y fácil
de escribir. En su forma impresa, las listas se componen de *elementos*, por
ejemplo:

```
(ROJO VERDE AZUL)
(2 3 5 7 11 13 17)
```

*Representación interna*: Es la forma en la que la lista existe en la memoria
de la computadora y es representada por una notación gráfica[^1], organizadas
como cadenas de *celdas cons* enlazadas por *punteros*. Cada celda cons tiene
dos punteros, uno de los cuales siempre apunta al elemento de la lista y el
otro apunta a la siguiente celda cons.

[^1]: Esta notación es obtenida empleando `SDRAW`, la cual puede ser encontrada
en [éste enlace](https://www.cs.cmu.edu/~dst/Lisp/sdraw/sdraw.generic), para
invocarla hay que copiar y pegar el código de la página en el REPL,
posteriormente podemos mostrar la representación de una lista escribiendo
`(sdraw lista)`.

```
[*|*]--->Segundo puntero
 |
 v
 Primer puntero

Por ejemplo:
[*|*]--->[*|*]--->[*|*]--->NIL
 |        |        |
 v        v        v
ROJO     VERDE    AZUL
```

## Listas anidadas

Una lista puede contener a otra lista como elemento, por ejemplo, la lista
`((cielo azul) (pasto verde) (tierra cafe))` contiene tres elementos en el
primer nivel, y cada subnivel contiene dos elementos. Su representación interna
es la siguiente:

```
[*|*]------------------>[*|*]------------------>[*|*]--->NIL
 |                       |                       |
 v                       v                       v
[*|*]--->[*|*]--->NIL   [*|*]--->[*|*]--->NIL   [*|*]--->[*|*]--->NIL
 |        |              |        |              |        |
 v        v              v        v              v        v
CIELO    AZUL           PASTO    VERDE          TIERRA   CAFE
```

> Nota: Las listas que no están anidadas como `(ROJO VERDE AZUL)` son llamadas
> listas "planas".


## Longitud de las listas

La longitud de una lista es el número de elementos que tiene en el primer
nivel, por ejemplo la lista `((A) (B C) (D))` contiene tres elementos.

```
[1er elemento]      [2do elemento]          [3er elemento]
[*|*]-------------->[*|*]------------------>[*|*]--->NIL
 |                   |                       |
 v                   v                       v
[*|*]--->NIL        [*|*]--->[*|*]--->NIL   [*|*]--->NIL
 |                   |        |              |
 v                   v        v              v
 A                   B        C              D
```

La función primitiva `LENGTH` devuelve la longitud de una lista y causará error
en caso de recibir símbolos o números como entradas.

## NIL: La lista vacía 

Una lista de cero elementos es llamada una _lista vacía_. Se representa como un
par de paréntesis `()` o por el símbolo `NIL`; y es usada como final de una
cadena en las celdas cons. Por ejemplo, la lista `(A NIL B)`, puede también ser
escrita como `(A () B)`.

## Igualdad de listas

Dos listas son consideradas iguales si sus elementos son iguales. Por ejemplo,
las listas `(A (B C) D)` y `(A B (C D))`, mostradas abajo tienen el mismo
número de elementos en su primer nivel, pero no son iguales. 

```
[*|*]--->[*|*]------------------>[*|*]--->NIL
 |        |                       |
 v        v                       v
 A       [*|*]--->[*|*]--->NIL    D
          |        |
          v        v
          B        C

[*|*]--->[*|*]--->[*|*]--->NIL
 |        |        |
 v        v        v
 A        B       [*|*]--->[*|*]--->NIL
                   |        |
                   v        v
                   C        D
```

`EQUAL` es una función *predicado* de Lisp que determina la igualdad de dos
argumentos, en este caso listas: 

```
* (equal '(a b c) '(a b c)) ; => T

* (equal '(a (b c) d) '(a b c d)) ; => NIL
```

## Funciones primitivas

Lisp provee funciones primitivas para extraer elementos de una lista. Algunos
de ellos son `FIRST`, `SECOND` y `THIRD`, que regresan el primer, segundo, y
tercer elemento de su entrada, respectivamente.

```
(first '(a b c)) ; => A
(second '(a b c)) ; => B
(third '(a b c)) ; => C
```

> Nota: Produce un error dar a estas funciones entradas que *no* sean listas.

`REST` es una función complemento de `FIRST`, ya que devuelve todo lo que no
sea el primer elemento.

```
* (rest '(a b c))
(B C)
```

> Usar `FIRST` en conjunto con `REST`, devolverá la lista completa.

## Las funciones operan sobre apuntadores

La entrada a una función no es un objeto (sea lista o símbolo), si no un
apuntador al objeto. Del mismo modo, lo que devuelve una función es un
apuntador (a un objeto).

```
       | El resultado            |
       | de la función REST      |
       | es también un apuntador |
          | 
          v 
[*|*]--->[*|*]--->[*|*]--->NIL 
 |        |        | 
 v        v        v 
LA      GRAN      COSA 
``` 

## CAR y CDR 

Cada mitad de una celda cons cuenta con un apuntador, la mitad izquierda es
llamada el `CAR` y la derecha el `CDR`, por lo que, cuando una lista se utiliza
como entrada a una función, en este caso `CAR` o `CDR`, lo que la función
recibe no es la propia lista, sino un puntero a la primera celda cons:

```
| Entrada         |
| a las funciones |
| CAR y CDR       |
 |   
 v
[*|*]--->[*|*]--->[*|*]--->NIL
 |        |        |
 v        v        v
LA      GRAN      COSA
```

La diferencia para `CAR` y `CDR` radica en el apuntador que devuelven. En el
caso de `CAR`, toma el apuntador de la primera celda cons, devolviendo el
símbolo `LA`. Por otro lado `CDR` toma el segundo apuntador de la primera celda
cons, devolviendo (a diferencia de `CAR`) una lista: `(GRAN COSA)`. 

```lisp
(sdraw (car '(la gran cosa)))
LA                    ; Devuelve un símbolo

(sdraw (cdr '(la gran cosa)))
[*|*]--->[*|*]--->NIL ; Devuelve una lista
 |        |
 v        v
GRAN     COSA
```

### Combinaciones de CAR y CDR

Existen funciones predefinidas en Lisp capaces de leer hasta cuatro
combinaciones de `CAR` y `CDR`, por ejemplo: `CAAR, CADR, CDAR, CDDR, CAAAR,
... CADDDR`. 

Esta forma de acceder a elementos de las listas es útil en caso de tener listas
anidadas, por ejemplo el `CADR` y el `CDAR` de la lista `((cubo azul) (piramide
roja))` son respectivamente:

```lisp
(cadr '((cubo azul) (piramide roja))) ; => (PIRAMIDE ROJA)

(cdar '((cubo azul) (piramide roja))) ; => AZUL
```

> Nota: Para poder entender cómo se leen y ejecutan estas funciones, es necesario leer y aplicar las funciones de derecha a izquierda.

### CONS

La función `CONS` crea celdas cons. Esta recibe dos entradas y devuelve un
apuntador a un nuevo par de celdas cons, cuyo `CAR` apunta a la primera entrada
y cuyo `CDR` apunta a la segunda.

```lisp
(cons 1 '(2 3)) ; => (1 2 3)
```

Podemos crear una lista a partir de un elemento y `NIL`, o anidar listas, por
ejemplo:

```lisp
(cons 1 '()) ; => (1)
(cons '(1) (2 3)) ; => ((1) 2 3)
```

### LIST

Otra forma de crea listas es con la función `LIST`, a diferencia de `CONS`,
`LIST` crea una celda cons para cada elemento, de esta forma se crea una nueva
cadena.

```lisp
(list 'a 'b '(juan)) ; => (A B (JUAN))

(sdraw (list 'a 'b '(juan)))
[*|*]--->[*|*]--->[*|*]--->NIL
 |        |        |
 v        v        v
 A        B       [*|*]--->NIL
                   |
                   v
                  JUAN
```

### APPEND

La función `APPEND` crea una lista a partir del contenido de las listas que se
le pasen como argumentos. Por ejemplo,

```lisp
(append '(1 2 3) '(4 5 6)) ; => (1 2 3 4 5 6)
```

### Predicados de listas

`LISTP` es un predicado que devuelve `T` si su entrada es una lista.

```lisp
(listp '(a b c)) ; => T
```

`CONSP` es similar a `LISTP`, sin embargo, si `CONSP` recibe una lista vacía
devolverá `NIL`, debido a que `NIL` no es una celda cons.

```lisp
(consp '()) ; => NIL
(consp '(a . b)) ; => T
(consp '(1 2 3)) ; => T
```

El predicado `ATOM` devuelve `T` si su entrada es distinta a una celda cons.
`CONSP` es complementario a `ATOM`.

```lisp
(atom nil) ; => T
(atom '()) ; => T
(atom '(x y z)) ; => T
```

> El concepto *átomo* en LISP se refiere a algo indivisible, por ejemplo, los
> números o símbolos. Las listas *no* cumplen con esta definición, pues pueden
> fragmentarse con funciones como `FIRST` o `CONS`.

`NULL` es otro predicado que retorna `T` si la entrada es `NIL`, es decir, vacía.

```lisp
(null '()) ; => T
(null 'lisp) ; => NIL
```

Para realizar comparaciones lógicas se recomienda emplear `NOT`, el cual
devuelve el complemento de un booleano.

```lisp
(not t) ; => NIL
(not nil) ; => t
```
