;; 4.15
(defun mayor-igual-que (x y)
  "Devuelve 'T' si la primer entrada 'X' es mayor o igual a 'Y'"
  (or (> x y) (equal x y)))
;; 4.16
(defun cuadrado-o-doble (x)
  "Eleva al cuadrado un número 'X' si es impar, lo divide entre 2 si es par"
  (if (oddp x)
      (* x x)
      (/ x 2)))
;; 4.17
(defun chicx-adultx-p (persona etapa)
  "Devuelve 'T' si su primer entrada es 'CHICO' o 'CHICA' y
   la segunda es 'CHICX', o si su primer entrada es 'HOMBRE'
   o MUJER' y la segunda es 'ADULTX'"
  (or (and (or (eq persona 'chico) (eq persona 'chica))
           (eq etapa 'chicx))
      (and (or (eq persona 'hombre) (eq persona 'mujer))
           (eq etapa 'adultx))))
;; 4.18
;; Escribir una función que actúa como mediador en el juego
;; "Piedra, papel o tijera". La función PLAY (JUGAR) tomará dos entradas,
;; de la cuales cada una será ROCK (PIEDRA), SCISSORS (TIJERAS) O
;; PAPER (PAPEL), y deberá devolver el símbolo FIRST-WINS (PRIMERO-GANA),
;; SECOND-WINS (SEGUNDO-GANA) o TIE (EMPATE).
(defun piedra-papel-tijeras (eleccion1 eleccion2)
  "Función que es un juez del juego piedra papel o tijera"
  (cond
    ((or (and (eq eleccion1 'papel)   (eq eleccion2 'piedra))
         (and (eq eleccion1 'tijeras) (eq eleccion2 'papel))
         (and (eq eleccion1 'piedra)  (eq eleccion2 'tijeras))) 'primerx-gana)
    ((or (and (eq eleccion2 'papel)   (eq eleccion1 'piedra))
         (and (eq eleccion2 'tijeras) (eq eleccion1 'papel))
         (and (eq eleccion2 'piedra)  (eq eleccion1 'tijeras))) 'segundx-gana)
    (t 'empate)))
;; 4.19
;; Escribir la expresión (AND X Y Z W) usando COND en lugar de AND. Después
;; mostrar cómo escribirla usando IFs anidados en lugar de AND.
;; 4.20
;; Escribir una versión de la función COMPARE usando IF en lugar de COND.
;; Escribir otra versión usando AND y OR.
;;
;; 4.21
;; Escribir versiones de la función GTEST usando IF y COND
;;
;; 4.22
;; Usa COND para escribir el predicado BOILINGP (HIRVIENDOP) que toma dos
;; entradas, TEMP y SCALE (ESCALA), esta retorna T si la temperatura es
;; mayor al punto de hervor del agua. Si la escala es FAHRENHEIT el punto
;; de hervor es 212 grados; si es CELCIUS es 100 grados. Escribir una
;; versión usando IF y AND/OR en lugar de COND
;;
;; 4.23
