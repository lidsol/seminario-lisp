# Notación EVAL

La notación EVAL permite representar las expresiones como listas e interpretarlas.

Los argumentos de la función se componen de una lista de símbolos que dan nombre a sus entradas, las cuales pueden ser referidas dentro del cuerpo de la función. Si damos a `EVAL` la expresión `(+ 2 3)` estamos invocando a la función `+` con las entradas 2 y 3, y se dice que dicha expresión se _evalua como_ 5.

En adelante, en vez de usar cajas para representar las expresiones, usarémos solo una flecha o una flecha de dos puntas para ser más específicas:

```
(+ 2 3) => 5

 --> (+ 2 3)
|
 --> 5
```

Y para especificar tanto detalle como sea posible, se usa una flecha en negrita que podemos visualizar así:

```
 --> (+ 2 3)
|
| ==> Introduce + con entradas 2 y 3
||
  ==> Resultado de + es 5
```

## Reglas de evaluación

La forma en que `EVAL` evalua las funciones tiene ciertas reglas:

- **Números y algunos objetos se pueden _autoevaluar_**, ejemplos: ` 1 => 1`, `t => t`, `nil => nil`.
- Para evaluar listas:
  - **El primer elemento de una lista es la función a llamar**;
  - **los elementos restantes son los argumentos de la función**, que deberán ser evaluados de izquierda a derecha, para determinar el orden de entrada de los argumentos a la función. Por ejemplo: `(ODDP (+ 1 6))`, primero evalua el argumento de `ODDP`, que es la lista `(+ 1 6)`, para lo cual evaluamos primero los argumentos de `+`. Como _1 se evalua como 1_ y _6 se evalua como 6_, ahora podemo llamar a la función `+` con entradas `1` y `6`, lo cual devuelve `7`. Éste último valor es el que sirve de entrada para `ODDP`, que devuelve `T`.

## Definición de funciones en notación EVAL

En notación EVAL se usan listas para definir funciones y nos referimos a sus argumentos dándoles nombres.

`DEFUN` es un tipo especial de función, llamada **macro función**, que no evalúa sus argumentos, si no que sirve para definir otras funciones:

```
(DEFUN nombre-de-funcion (lista-de-argumentos)
    cuerpo-de-funcion)
```

Por ejemplo, para definir la función cuadrado:

```
(defun cuadrado (n)
  (\* n n))
```

Estamos llamando a la función `CUADRADO`, su lista de argumentos es `(N)`, significa que acepta un valor al cual se refiere como `N`. El cuerpo de la función es (\* N N).

Cualquier símbolo excepto por `T` y `NIL` puede servir como nombre de un argumento.

_Nota: Las funciones son más legibles cuando los nombres de sus argumentos describen su función._

## Variables

Una **variable** es un lugar donde datos pueden almacenarse. El valor de una variable es el valor que alberga.

```lisp
(defun relacion (r c)
(/ r c))
```

Cuando se llama a la función `relacion`, Lisp crea dos nuevas variables `R` y `C` para contener las entradas, y que puedan ser referidas con ese nombre dentro del cuerpo de la función. Dentro del cuerpo de la función el símbolo `R` será la primera variable, y `C` la segunda variable. Estas variables _sólo_ pueden ser referenciadas dentro del cuerpo de la función; afuera de la función `RELACION` dichas variables son inaccesibles. El diagrama de evaluación muestra como `RELACION` calcula su resultado:

```lisp
 --> (relacion 50 19)
|        50 se evalúa como 50
|        20 se evalúa como 20
| ==> Ingresa RELACION con entradas 50 y 20
||        crea variable R, con valor 50
||        crea variable C, con valor 20
||  --> (/ r c)
|| |        R se evalúa como 50
|| |        C se evalúa como 20
||  --> 2.5
  ==> El resultado de RELACION es 2.5
```

Brevemente, la flecha delgada `-->` conecta una expresión con su valor, por ejemplo el resultado de `(/ r c)` es `2.5`. Mientras que la flecha doble `==>` se usa para mostrar las entradas/salidas al cuerpo de la función y lo que ocurre dentro de la misma.

_Nota: Es importante distinguir entre variables y símbolos, ya que no son lo mismo._

De manera generalizada la representación puede verse así:

```lisp
 --> (funcion argumento_1 ... argumento_n)
|        Evalúa los argumentos
| ==> Ingresa FUNCION con entradas (argumentos evaluados)
||      Crea las variables para contener los valores de las entradas
||  --> Cuerpo de la función
|| |
||  --> Valor obtenido del cuerpo de la función
  ==> El resultado de la FUNCION        es (valor)
```

## Reglas de evaluación para símbolos

Los nombres que una función usa para sus argumentos son independientes de los nombres que usa cualquier otra función.

> **Regla de evaluación para símbolos: Un símbolo se evalúa con el valor de la variable a la que se refiere.**

Una **variable global** es aquella que no está asociada a alguna función, por ejemplo `PI`, `pi => 3,14,159`.

## Uso de símbolos y listas como datos

En notación `EVAL` los símbolos son usados para nombrar variables, por lo que:
```lisp
(equal taza vaso)         ; => Error! TAZA unassigned variable.
(equal 'taza 'vaso)         ; => NIL
```
En la primera expresión, Lisp intenta comparar entre lo que "piensa" son variables globales que no han sido aún definidas, mientras que en el segundo caso, colocamos un `'` para indicar a Lisp que las trate como símbolos, dándoles un tratamiento de datos, no como variables.

Ya que `T` y `NIL` se evalúan a sí mismos, no es necesario precederles con `'`.

También será necesario preceder listas con `'` para tratarlas como datos, de otro modo Lisp intentará evaluarlas, lo que resultará en un error:
```lisp
(first (taza vaso)) ; => _Error! TAZA undefined function.
(first '(taza vaso)) ; => TAZA
```

>**Regla de evaluación para Objetos citados: Un objeto quoteado evalúa al objeto mismo sin la comilla.**

```
(third (bici de Juan)) => Error! BICI undefined function.
(third '(bici de Juan)) => Juan
```

## Errores por _quotear_ de manera incorrecta o no hacerlo

Los mensajes de error en Lisp dan buena pista sobre el error en caso de poner `'` en lugares incorrectos u omitirlos.

```
(list 'a 'b c) => Error! C unassigned variable.
(cons 'a (b c)) => Error! B undefined function.
(+ 10 '(- 5 2)) => Error! Wrong type inputo to +.
```
