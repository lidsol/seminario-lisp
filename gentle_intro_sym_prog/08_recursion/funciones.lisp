(defun anyoddp (x)
  (cond ((null x) ())
	((oddp (first x)) t)
	(t (anyoddp (cdr x)))))

(defun factorial (x)
  (cond
    ((zerop x) 1)
    (t (* x (factorial (1- x))))))

(defun algunimpar (x)
    (if (null x) nil 
        (if (oddp (first x)) t 
            (algunimpar (cdr x)))))

(defun cuenta-rebanadas (hogaza)
  (cond ((null hogaza) 0)
	(t (1+ (cuenta-rebanadas (cdr hogaza))))))

(defun risita (n)
	(cond ((zerop n) '())
	      (t (cons 'ja (risita (1- n))))))

(defun suma-recursiva (l)
  (cond
    ((null (car l)) 0)
    (t (+ (car l)
	  (suma-recursiva (cdr l))))))

(defun fib (n)
  (cond
    ((zerop n) 1)
    ((equal n 1) 1)
    (t (+ (fib (- n 1)) (fib (- n 2))))))

(defun any-7-p (x)
  (cond ((equal (car x) 7) t)
	(t (any-7-p (cdr x)))))

(defun _ ()
  (_))

(defun r ()
  (r))

(defun find-first-odd (list)
  (cond ((null list) ())
	((oddp (car list)) (car list))
	(t (find-first-odd (cdr list)))))

(defun last-element (list)
  (cond ((null (cdr list)) (car list))
	(t (last-element (cdr list)))))

(defun all-equal (list)
  (cond
    ((null (cdr list)) t)
    ((not (equal (car list) (cadr list))) ())
    (t (all-equal (cdr list)))))

(defun count-down (n)
  (cond ((zerop n) '(0))
	(t (cons n (count-down (1- n))))))

(defun square-list (list)
  (cond ((null list) ())
	(t (cons (* (car list) (car list)) (square-list (cdr list))))))

(defun my-nth (n list)
  (cond ((zerop n) (car list))
	((null list) ())
	(t (my-nth (1- n) (cdr list)))))

(defun compare-lengths (l1 l2)
  (cond ((and (null (cdr l1)) (null (cdr l2))) 'same-length)
	((null (cdr l1)) 'second-is-longer)
	((null (cdr l2)) 'first-is-longer)
	(t (compare-lengths (cdr l1) (cdr l2)))))

(defun extract-symbols (x)
  (cond ((null x) nil)
	((symbolp (car x)) (cons (car x) (extract-symbols (cdr x))))
	(t (extract-symbols (cdr x)))))

(defun suma-elementos-numéricos (lista)
  (cond ((null lista) 0)
	((numberp (car lista)) (+ (car lista) (suma-elementos-numéricos (cdr lista))))
	(t (suma-elementos-numéricos (cdr lista)))))

(defun count-atoms (tree)
  (cond ((atom tree) 1)
	(t (+ (count-atoms (car tree)) (count-atoms (cdr tree))))))

(defun count-cons (tree)
  (cond ((atom tree) 0)
	(t (+ 1
	      (count-cons (car tree))
	      (count-cons (cdr tree))))))

(defun sum-tree (tree)
  (cond
    ((numberp tree) tree)
    ((atom tree) 0)
    (t (+ (sum-tree (car tree))
	  (sum-tree (cdr tree))))))

(defun flatten (tree)
  (cond ((atom tree) (list tree))
	(t (append (flatten (car tree))
		   (and (cdr tree) (flatten (cdr tree)))))))

(defun memberp (e l)
  "Checa si el elemento e es miembro de la lista l."
    (cond
      ((null l) ())
      (t (or (equal (car l) e) (memberp e (cdr l))))))

(defun my-set-difference (l1 l2)
  "Devuelve la lista l1 menos los elementos de la lista l2"
  (cond ((null l1) '()) 
        ((memberp (car l1) l2) (my-set-difference (cdr l1) l2))
        (t (cons (car l1) (my-set-difference (cdr l1) l2)))))

(defun mi-assoc (x l)
  (cond
    ((null l) '())
    ((equal (caar l) x) (car l))
    (t (mi-assoc x (cdr lista)))))

(defun compara-longitud (l1 l2)
  (cond
    ((and (null l1) (null l2)) 'misma-longitud)
    ((and (not (null l1))
          (null l2)) 'primera-mas-larga)
    ((and (not (null l2))
          (null l1)) 'segunda-mas-larga)
    (t (compara-longitud (cdr l1) (cdr l2)))))


;; 8.4x
(defun profundidad-de-arbol (arbol-binario)
  (cond
    ((atom arbol-binario) 0)
    ((null arbol-binario) 0)
    (t (max (1+ (profundidad-de-arbol (car arbol-binario)))
        (1+ (profundidad-de-arbol (cdr arbol-binario)))))))


;; 8.45
;; Write a function PAREN-DEPTH that
;; returns the maximum depth of nested
;; parentheses in a list. (PAREN-DEPTH ‘(A B C))
;; should return one, whereas TREE-DEPTH would
;; return three. (PAREN-DEPTH ‘(A B ((C) D) E))
;; should return three, since there is an element C
;; that is nested in three levels of parentheses. Hint:
;; This problem can be solved by CAR/CDR
;; recursion, but the CAR and CDR cases will not
;; be exactly symmetric.

(defun profundidad-de-paréntesis (x)
    "Regresa la profundida máxima de listas anidadas."
  (cond
	((atom x) 0)
	((null (cdr x)) 1)
	(t (max (+ (profundidad-de-paréntesis (car x))
			(profundidad-de-paréntesis (cdr x)))))))

(1 2 3) (1 2 (2 3 4))

;;; 8.46 Otra forma de resolver el problema CONTAR-HASTA  es 
;;; añadir un elemento al final de la lista con cada llamada 
;;; recursiva, en vez de añadir los elementos al inicio. 
;;; Esta aproximación no requiere una función de ayuda. 
;;; Escribe esta versión de CONTAR-HASTA.

(defun cuenta-hasta (x)
        (cond ((zerop x) ())
        (t (append (cuenta-hasta (1- x)) (list x)))))
  
(trace cuenta-hasta)

(cuenta-hasta 4)
 
(list 4)
