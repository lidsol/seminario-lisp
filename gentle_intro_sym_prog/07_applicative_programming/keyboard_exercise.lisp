;;; Ejercicio de teclado
;;;
;;; 7.10

(defvar *NOTE-TABLE* '((C . 1) (C-SHARP . 2) (D . 3) (D-SHARP . 4) (E . 5) (F . 6) (F-SHARP . 7) (G . 8) (G-SHARP . 9) (A . 10) (A-SHARP . 11) (B . 12))
  "Tabla de correspondencia de notas musicales y sus números correspondientes.")

(defun numbers (lista)
  "Toma una lista y arroja la lista de números que corresponde según *note-table*."
  (mapcar #'cdr (mapcar #'(lambda (e) (assoc e *note-table*)) lista)))

(defun notes (lista)
  "Toma una lista de números y arroja la lista de notas que corresponde según *note-table*."
  (mapcar #'car (mapcar #'(lambda (e) (rassoc e *note-table*)) lista)))

(defun raise (n l)
  "Eleva en n a todo elemento de l."
  (mapcar #'(lambda (e) (+ e n)) l))

(defun normalize (lista)
  "Si un número es menor a 1 le suma 12 y si es mayor a 12 le resta 12"
  (mapcar #'(lambda (n) (cond ((< n 1) (+ 12 n)) ((> n 12) (- n 12)) (t n))) lista))

(defun normalize2 (lista)
  "doc"
  (mapcar #'normalize-auxiliar lista))

(defun normalize-auxiliar (n)
  "doc"
  (cond ((< n 1) (normalize-auxiliar (+ 12 n))) ((> n 12) (normalize-auxiliar (- n 12))) (t n)))

(defun transpose (n lista)
  "Transpone la canción n medios pasos (?)"
  (notes (normalize (raise n (numbers lista)))))


;;;
;;; 7.29
(defvar *base-de-datos*)
(setf *base-de-datos* '((b1 forma ladrillo)
                        (b1 color verde)
                        (b1 tamaño pequeño)
                        (b1 soportado-por b2)
                        (b1 soportado-por b3)
                        (b2 forma ladrillo)
                        (b2 color rojo)
                        (b2 tamaño pequeño)
                        (b2 soporta b1)
                        (b2 izquierda-de b3)
                        (b3 forma ladrillo)
                        (b3 color rojo)
                        (b3 tamaño pequeño)
                        (b3 soporta b1)
                        (b3 derecha-de b2)
                        (b4 forma pirámide)
                        (b4 color azul)
                        (b4 tamaño grande)
                        (b4 soportado-por b5)
                        (b5 forma cubo)
                        (b5 color verde)
                        (b5 tamaño grande)
                        (b5 soporta b4)
                        (b6 forma ladrillo)
                        (b6 tamaño grande)))

;;; Inciso a
;;; Escribir una función COINCIDENCIA-DE-ELEMENTO que recibe dos
;;; símbolos. Si ambos son iguales o si el segundo es un símbolo
;;; de interrogación COINCIDENCIA-DE-ELEMENTO deberá devolver T,
;;; regresará NIL en otro caso.
(defun coincidencia-de-elemento (simbolo-1 simbolo-2)
  (or (equal simbolo-1 simbolo-2)
      (equal simbolo-2 '?)))

(coincidencia-de-elemento 'red 'blue) ; => NIL
(coincidencia-de-elemento '? 'red) ; => NIL
(coincidencia-de-elemento 'red 'red) ; => T
(coincidencia-de-elemento 'red '?) ; => T

;;; Inciso b
;;; Escribir una función COINCIDENCIA-TRIPLE que toma una afirmación
;;; y un patrón como entrada, regresa T si la afirmación coincide con
;;; el patrón. Los argumentos son listas de tres átomos cada una.
(defun coincidencia-triple (afirmación patrón)
  (every #'coincidencia-de-elemento afirmación patrón))

(coincidencia-triple '(b1 color rojo) '(? color rojo)) ; => T

;;; Inciso c
;;; Escribe la función BUSCAR que recibe un patrón y devuelve todas
;;; las conicidencias que encuentre en la base de datos.

(defun buscar (patrón)
  (remove-if-not #'(lambda (afirmación)
                     (coincidencia-triple afirmación patrón))
                 *base-de-datos*))

(buscar '(b4 forma ?)) ; => ((B4 FORMA PIRÁMIDE))

;;; Inciso e (esto lo armo @mikra )
;;; Escribe una función que tome el nombre de bloque como entrada
;;; y regrese un patrón preguntando el color del bloque. 
;;; Por ejemplo, dada una entrada B3, tu función debe regresar 
;;; la lista (B3 color ?)
(defun pregunta-color (bloque)
    (append bloque '(color ?)))

;;; Recibe un bloque y regresa la lista de los bloques
;;; que lo soportan si existe. (Intento fallido del inciso g).
;;; (defun soportes (bloque)
;;;  (mapcar #'first (funcall #'buscar (list '? 'soporta bloque))))

;;; Inciso g
;;; Escribe el predicado SOPOR-CUBOP que toma un bloque como entrda
;;; y devuelve T si el bloque es soportado por un cubo, NIL en otro caso.
;;; Construcción de la función:
(car (buscar '(b4 soportado-por ?))); => (B4 SOPORTADO-POR B5)
;;; Tomamos NTH 2 del resultado anterior, que es B5, y lo buscamos para
;;; conocer su forma:
(buscar '(b5 forma ?)) ; => ((B5 FORMA CUBO))
;;; Si es un cubo, el resultado será distinto a NIL...
(null (buscar '(b5 forma cubo))) ; => NIL
(not (null (buscar '(b5 forma cubo)))) ; => T

(defun sopor-cubop (bloque)
  (not
   (null
    (buscar (list (nth 2 (car (buscar (list bloque 'soportado-por '?))))
                  'forma
                  'cubo)))))
(sopor-cubop 'b4) ; => T
(sopor-cubop 'b3) ; => NIL

;;; Inciso h
;;; Escribe DESCRIPCION1, que devuelve la descripción de un bloque.
(defun descripcion1 (bloque)
  (buscar (list bloque '? '?)))

(descripcion1 'b3);  => ((B3 FORMA LADRILLO) (B3 COLOR ROJO) (B3 TAMAÑO PEQUEÑO) (B3 SOPORTA B1)
 ; (B3 DERECHA-DE B2))

;;; Inciso i
;;; Escribe DESCRIPCION2, que llama a DESCRIPCION1 y le quita el nombre
;;; del bloque a la lista.

(defun descripcion2 (bloque)
  (mapcar #'cdr (descripcion1 bloque)))

(descripcion2 'b1);  => ((FORMA LADRILLO) (COLOR VERDE) (TAMAÑO PEQUEÑO) (SOPORTADO-POR B2)
 ; (SOPORTADO-POR B3))

;;; Inciso j
;;; Escribe DESCRIPCION. Toma una entrada, llama a DESCRIPCION2 y une
;;; los resultados de la lista de listas en un única lista.
(defun descripción (bloque)
  (reduce #'append (descripcion2 bloque)))

;;; Inciso k
(descripción 'b1) ; => (FORMA LADRILLO COLOR VERDE TAMAÑO PEQUEÑO SOPORTADO-POR B2 SOPORTADO-POR B3)
(descripción 'b4) ; => (FORMA PIRÁMIDE COLOR AZUL TAMAÑO GRANDE SOPORTADO-POR B5)

;;; Inciso l
;;; El bloque B1 está hecho de madera, B2 está hecho de plástico.
;;; ¿Cómo agregamos esta información a la base de datos?.
;;; Una versión de member que funciona con listas (member no):
(defun member-lista (lista1 lista2)
  (cond
    ((equal lista2 ()) ())
    ((equal lista1 (car lista2)) lista2)
    (t (member-lista lista1 (cdr lista2)))))
;;; Devolvemos una base de datos con la primera mitad de donde se
;;; encuentra LUGAR con DATO y posteriormente unimos la segunda mitad.
(defun agrega-datos (dato lugar base-de-datos)
  (append
   (reverse (cons dato
                  (member-lista (car (buscar (list lugar '? '?))) (reverse base-de-datos))))
   (cdr (member-lista
         (car (buscar (list lugar '? '?)))
         base-de-datos))))
