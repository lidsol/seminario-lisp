# Programación aplicativa

La programación aplicativa tiene como idea fundamental que las funciones son
datos, así como los símbolos y las listas lo son. Con esto se pueden pasar
funciones como entradas de otras funciones y regresar funciones como valores.

Los _operadores aplicativos_ o _applicative operators_ son funciones aquellos 
que reciben una función y la aplican a los elementos de una lista de diferentes
formas. Estos operadores se derivan de la función primitiva llamada `FUNCALL`.

Con `FUNCALL` llamamos una función con ciertos parámetros

```lisp
(funcall #'cons 'a 'b) ; => (a . b)
```

Los símbolos `#'` o _sharp quote_ es la notación adecuada para hacerle _quote_
a una función en _Common Lisp_[^1]. En otras palabras podemos obtener una función
por su nombre lo cual puede ser conveniente en ciertos casos.

```lisp
(setf fn #'cons)
(type-of fn) ; Compiled-function CONS
(funcall fn 'c 'd) ; => (c . d)
```

De esta manera estaríamos "guardando" la función `CONS` en una variable llamada
`fn`. Entonces, `fn` es un objeto de tipo función (como lo muestra `type-of`).
Cabe destacar que los símbolos y las funciones no son lo mismo. `CONS` es un
símbolo que sirve como nombre para la función `CONS` pero no es en realidad la
función. Podríamos decir que es una etiqueta para poder identificar la función.
Existe `CONS` la etiqueta y `CONS` la función.

[^1]: Solo las funciones ordinarias pueden ser candidatas a _quoted_ con los
símbolos `#'`. Por ejemplo, si se desea hacer _quote_ a una macro o una función
especial tendremos un error.

```lisp
#'if ; Error
```

## Operador `MAPCAR`

El operador aplicativo `MAPCAR` aplica una función a cada elemento de una lista,
uno a la vez, y regresar una lista con el resultado. Por ejemplo, elevar al
cuadrado cada número de una lista. La función que escribimos para elevar al cuadrado
solo funciona con número individuales no con listas completas.

```lisp
(defun square (n) (* n n))
(square 3) ; => 9
(square '(1 2 3 4 5)) ; ERROR :(
```

La solución a este problema es usar `MAPCAR` para aplicar la función `SQUARE` a
cada elemento de la lista. Para pasar la función `SQUARE` debemos usar los simbolos
`#'` con lo que haríamos _quote_ a la función.

```lisp
(mapcar #'square '(1 2 3 4 5)) ; +> (1 4 9 16 25)
```

## Operaciones con tablas y `MAPCAR`

Sea una tabla con palabras en español y otomí: 

```lisp
 (setf palabras
    '((azul ixki)
      (blanco t\'äxi)
      (amarillo k\'ats\'i)
      (negro mbo\'i)
      (verde k\'angi)))
```
>> Fuente: https://elotl.mx/proyectos/materiales-didacticos/infografias-otomi/ 

Gracias al operador `MAPCAR` podemos hacer manipulaciones de tablas como las que
se muestran acontinuación:

```lisp
; Extraer una lista de palabras en Español
(mapcar #'first palabras) ; => (azul blanco amarillo negro verde)
```

```lisp
; Extrar una lista de palabras en otomí
(mapcar #'second palabras) ; => (IXKI |T'ÄXI| |K'ATS'I| |MBO'I| |K'ANGI|)
```

```lisp
; Crear un diccionario otomi-español a partir de la tabla español-otomi
(mapcar #'reverse palabras)
```

### Ejercicios

7.1 Escribe una funcion `ADD1` que sume uno a su entrada. Despues escribe una
expresion que sume uno a cada elemento de la lista `(13 5 7 9)`

```lisp
(defun add1 (x) (+ x 1))

(mapcar #'add1 '(13 5 7 9))

```

7.3 Escribe una expresion que aplique el predicado `ZEROP` a cada elemento de
la lista `(2 0 3 4 0 -5 -6)`. La respuesta que tendras deberá ser una lista de
`T`s y `NIL`s.

```lisp
(mapcar #'zerop '(2 0 3 4 0 -5 -6))
```

## Expresiones Lambda

Además de identificar a traves del nombre la función que el operador aplicativo
usará, de la forma `#'name` existe una segunda forma que es pasarle la función
directamente al operador. Esto se hace escribiendo lo que se conoce como
__expresiones lambda__. Por ejemplo:

```lisp
(lambda (n) (* n n)) ; lambda que computa el cuadrado de la entrada n
```

Ya que las __expresiones lambda__ son funciones pueden ser pasadas directamente
a `MAPCAR` con el simbolo `#'`. Esto evita que tengamos que definir la función
previamente con `DEFUN`.

```lisp
(mapcar #'(lambda (n) (* n n)) '(1 2 3 4 5)) ; => (1 4 9 16 25)
```

Las __expresiones lambda__ son funciones sin nombre o anónimas. El uso de la
palabra reservada `LAMBDA` es un marcador para indicar que la lista que viene
acontinuación es una función por lo que no es una macro o función especial como
`DEFUN`. El libro se refiere a las expresiones del tipo `#'(LAMBDA...)` como
__lexical closures__.

### Ejercicios

7.5 Escribe una expresion lambda que reste siete a un numero

```lisp
; (n - 7)
; (- n 7)
(lambda (n) (- n 7))
```

## El operador `FIND-IF`

Otro operador aplicativo es `FIND-IF`. Lo que recibe es un predicado y una
lista como entradas, encontrara el primer elemento de la lista que haga que el
predicado devuelva `true` (o cualquier valor no `NIL`). `FIND-IF` regresa ese
valor.

```lisp
(find-if #'oddp '(2 4 6 7 8 9)) ; => 7
(find-if #'(lambda (x) (> x 3)) '(2 4 6 7 9)) ; => 4
```

Si ningun elemento satisface el predicado, `FIND-IF` regresa `NIL`.

## Reescribiendo `ASSOC` con `FIND-IF`

```lisp
; ((1 2)
;  (1 2))
  
(defun neoassoc (key table)
    (find-if #'(lambda (entry) (equal key (first entry))) table))

(neoassoc 'azul palabras) ; => (azul ixki)
```

La expresion lambda (_lexical clousure_) recibe elementos de la tabla del estilo
`(AZUL IXKI)`. Regresa `T` si el primer miembro del elemento en turno es igual a
la llave (`key`) que recibe la función `NEOASSOC`. `FIND-IF` llama el `clousure`
por cada entrada de la tabla, hasta que encuentra una que el `clousure` devuelva
`T`.

Cabe destacar que en la parte de código `(equal key (first entry))` del cuerpo
de la expresion lambda aparecen dos variables; `KEY` y `ENTRY` con lo que
muestra una caracteristica importante de las expresiones lambda: Dentro del
cuerpo de la expresion lambda se puede hacer referencia a las variables locales
de la misma expresion asi como a las variables locales de la funcion que
contiene dicha expresion lambda.

### Ejercicios

#### Mini Keyboard excercise

##

```lisp
(defun count-zero (l) 
    (length (remove-if-not #'zerop l)))
```

## `REMOVE-IF` y `REMOVE-IF-NOT`

Otro operador aplicativo es `REMOVE-IF` que toma un predicado como entrada.
`REMOVE-IF` elimina todos los elementos de la lista que satisfagan el predicado
y regresa una lista con lo que queda:

```lisp
(remove-if #'numberp '(2 for 1 sale)) ; (FOR SALE)
(remove-if #'oddp '(1 2 3 4 5 6 7)) ; (2 4 6)
```

Suponiendo que queremos encontrar todos los números positivos de una lista de
números. El predicado `PLUSP` prueba si un numero es mayor a cero. Para
invertir el predicado (probar los números que **no** son mayores a cero)
podemos usar la función `NOT`, como se muestra a continuación.

```lisp
(remove-if #'(lambda (x) (not (plusp x)))
						'(2 0 -4 6 -8 10))
; (2 6 10)
```

Con esto lo que resta son todos los elementos positivos ya que elimina todos
los que no son mayores a cero.

El operador `REMOVE-IF-NOT`, que es usado con mas frecuencia, automaticamente 
revierte el sentido del predicado. Esto significa que los elementos removidos 
serán solo aquellos que regresen `NIL` al ser probados con el predicado.
Entonces, si decidimos utilizar `PLUSP` como predicado, `REMOVE-IF-NOT` dejará
todos los números positivos de la lista.

```lisp
(remove-if-not #'plusp '(2 0 -4 6 -8 10)) ; => (2 6 10)
(remove-if-not #'oddp '(2 0 -4 6 -8 10)) ; => NIL
```

Ejemplos adicionales de `REMOVE-IF-NOT`:

```lisp
(remove-if-not #'(lambda (x) (> x 3))
                '(2 4 6 8 4 2 1)) ; => (4 6 8 4)
(remove-if-not #'numberp '(3 manzanas 4 peras y 2 pequeñas plumas)) 
; => (3 4 2)
(remove-if-not #'symbolp '(3 manzanas 4 peras y 2 pequeñas plumas))
; => (MANZANAS PERAS Y PEQUEÑAS PLUMAS)
```

Por último, una función llamada `COUNT-ZEROS` que cuenta cuantos ceros aparecen
en una lista de números. Esta funcion toma un subconjunto de elementos que son
ceros y luego calculamos la longitud del resultado.

```lisp
(remove-if-not #'zerop '(34 0 0 95 0))
(defun count-zeros (x)
  (lenght (remove-if-not #'zerop x)))
(count-zeros '(0 0 0 0 0)) ; => 5
(count-zeros '(34 0 0 95 0)) ; => 3
(count-zeros '(1 2 3 4 5)) ; => 0
(count-zeros '(1 0 63 0 38)) ; => 2
```

## El operador `REDUCE`

`REDUCE` es un operador aplicativo que reduce los elementos de una lista en un
resultado único. `REDUCE` toma una función y una lista como entradas, pero a
diferencia de los otros operadores, `REDUCE` necesita una función que acepte
**dos** elementos de entrada. Por ejemplo, para sumar los elementos de una
lista con `REDUCE` usamos `+` como función reductora.

```lisp
(reduce #'+ '(1 2 3)) ; => 6
(reduce #'+ '(10 9 8 7 6)) ; => 40
(reduce #'+ '(5)) ; => 5
(reduce #'+ nil) ; => 0
```

Similar, para multiplicar una lista de números, usaremos `*` como función
reductora:

```lisp
(reduce #'* '(2 4 5)) ; => 40
(reduce #'* '(3 4 0 7)) ; => 0
(reduce #'* '(8)) ; => 8
```

Tambien podemos aplicar reducción a lista de listas. Para convertir una tabla
en una lista de un solo nivel podemos usar `APPEND` como función reductora:

```lisp
(reduce #'append '((azul ixki) (blanco t\'äxi) (amarillo k\'ats\'i)))
```

## `EVERY`

Este operador toma un predicado y una lista como entradas. Devolvera `T` si no
hay ningún elemento que cause que el predicado sea falso. Por ejemplo:

```lisp
(every #'numberp '(1 2 3 4 5 6)) ; T
(every #'numberp '(1 2 A B 5 6)) ; F
(every #'(lambda (x) (x > 0) '(1 2 3 4 5))) ; T
(every #'(lambda (x) (x > 0) '(1 2 3 -4 5))) ; F
```

## Resumen

* Los operadores aplicativos son funciones que aplican otras funciones a
  estructuras de datos. Existen muchos operadores aplicativos y solo algunos
  están incorporados en Lisp.
* Las programadoras avanzadas de Lisp hacen sis propios operadores conforme los
  van necesitando.
* `MAPCAR` aplica una función a cada elemento de una lista y regresa una lista
  como resultado
* `FIND-IF`recibe un predicado y una lista. Buscará la lista y devolvera el
  primer elemento que satisfaga el predicado.
* `REMOVE-IF` recibe un predicado y una lista. Remueve los elementos de la lista
  que satisfagan el predicado, entonces dejará solo los que fallen en satisfacer
  la condición. `REMOVE-IF-NOT` es el inverso de `REMOVE-IF` dejando los
  elementos que si satisfagan el predicado.
* `REDUCE` usa una función reductora para reducir una lista a un solo elemento.
* `EVERY` devuelve `T` solo si cada elemento de la lista satisface un predicado.

## Funciones vistas en este capitulo

* `MAPCAR`
* `FIND-IF`
* `REMOVE-IF`
* `REMOVE-IF-NOT`
* `REDUCE`
* `EVERY`

# Temas avanzados 

## Operando en listas multiples

Al inicio utilizamos `MAPCAR` para aplicar funciones de un argumento a los
elementos de una lista. Sin embargo, `MAPCAR` no se restringe a funciones de una
entrada. Dada una función de *n* argumentos, `MAPCAR` va a mapearlos sobre *n*
listas. Por ejemplo, si tenemos una lista de personas y otra de trabajos podemos
usar `MAPCAR` con una función de *2* argumentos para crear pares de
persona-trabajo:

```lisp
(mapcar #'(lambda (x y) (list x '<=> y)) 
  '(Juan Enrique Diego Mikra) 
  '(Bajista Vocalista Guitarrista Batertista))
```

`MAPCAR` va a traves de las dos listas en paralelo, tomando un elemento de cada
una. Si alguna lista es menor que la otra, `MAPCAR` para cuando llegue al final
de la mas corta.

Otro ejemplo donde se opera en multiples listas es cuando queremos sumar los
elementos de dos listas en pares:

```lisp
(mapcar #'+ '(1 2 3 4 5) '(10 20 30 40 50))
(mapcar #'+ '(1 2 3) '(10 20 30 40 50))
```

## La funcion especial `FUNCTION`

Asi como `'` es un *shorhand* de la función especial `QUOTE`, el simbolo `'#` es
un *shorthand* para la función especial `FUNCTION`. Por tanto, escribir `#'cons`
es equivalente a escribir `(FUNCTION CONS)`. 

Mientras que `QUOTE` devuelve su argumento sin evaluar, `FUNCTION` funciona de
una forma un tanto distinta. Este regresa una **interpretación funcional**
(*functional interpretation*) de su argumento sin evaluar. Si el argumento es un
simbolo, devuelve el contenido de la celda de funcion (*function cell*) del
simbolo. A menudo es codigo objeto compilado:

```lisp
> 'cons ; CONS
> #'cons ; <Compiled-function CONS 6041410>
```

Por otro lado, si el argumento a `FUNCTION` es una expresión lambda, el
resultado es una *lexical closure*:

```lisp
#'(lambda (x) (> x 0)) ; #<Lexical-closure 3471524>
```

El resultado devuelto por `FUNCTION` siempre sera un tipo de objeto función.
Dichos objetos son una forma de datos, como las listas y los simbolos.
Entonces, podemos guardar estos objetos en variables y llamarlos como funciones
usando `FUNCALL` o `APPLY`. (`APPLY` fue vista en los temas avanzados del
capitulo 3)

```lisp
(setf g #'(lambda (x) (* 10 x)))
(funcall g 12) ; 120
```

el valor de la variable `G` es una *lexical closure* que es una función. Pero
`G` por si misma no es el nombre de una función; si escribimos `(G 12)`
tendremos un error *undefined function error*.

## Argumentos de palabras clave para los operadores aplicativos

Algunos operadores aplicativos, como `FIND-IF`, `REMOVE-IF`, `REMOVE-IF-NOT`
y `REDUCE`, aceptan argumentos de palabras clave (*keyword arguments*)
opcionales. Por ejemplo, si la palabra clave `:FROM-END` recibe un valor no
`NIL`, causara que la lista sea procesada de derecha a izquierda.

```lisp
(find-if #'oddp '(1 2 3 4 5 6)) ; 1
(find-if #'oddp '(1 2 3 4 5 6) :from-end t) ; 5
```

`:FROM-END` es particularmente interesante en `REDUCE`:

```lisp
(reduce #'cons '(a b c d e))
(reduce #'cons '(a b c d e) :from-end t)
```

`REMOVE-IF` y `REMOVE-IF-NOT` ademas aceptan la palabra clave `:COUNT` que
especifica el número maximo de elementos a remover. Para más información acerca
de palabras clave visita el manual de referencia de la implementación de Common
Lisp que estes utilizando.

## Alcance (*scoping*) y *lexical clousures*

TODO...

## Escribiendo un operador aplicativo

TODO...

## Funciones que crean funciones

TODO...

# Lisp Toolkit: `TRACE` y `DTRACE` 

TODO...
