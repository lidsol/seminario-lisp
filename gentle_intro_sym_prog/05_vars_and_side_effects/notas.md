# Variables y efectos secundarios

Las variables se emplean para almacenar información. Existen variables globales y locales. Las globales se crean fuera de las funciones mientras que las variables locales son creadas con `let` o en los argumentos de la función.

`setf` permite asignar un valor a una variable o cambiar el valor de una.

## Variables globales y locales

En los lenguajes de programación se tiene el concepto de _área de alcance_ de una variable (scope), este nos permite escribir el nombre de una variable y obtener su valor de acuerdo al área de alcance de la variable.

Las funciones que definen variables en su cuerpo emplean *variables locales*, pues estas únicamente existen dentro del cuerpo de la función.

Las funciones declaradas con `defvar` y `setf` fuera de alguna función tienen un área de alcance global, es decir existen tanto dentro como fuera de funciones, se les conoce como *variables globales*.

> Nota: Se recomienda asignar `*` (_earmuffs_ u _orejeras_) alrededor del nombre de la variable global para diferenciarla de variables locales.

```lisp
(defun cuadrado (x) ; 'x' es variable local
 (* x x)) ; => CUADRADO

(cuadrado 3) ; => 9
x ; => ERROR! N unassigned variable

(defvar *y*) ; => Y
(setf *y* 5) ; => 5
(cuadrado *y*) ; => 25
*y* ; => 5
;;; '*y*' es variable global.
```

## Asignando valores con `setf`

`setf` es una macro usada para asignar datos a una variable o modificar el valor de una. Se suele emplear para variables globales.

```lisp
(defvar vocales) ; => VOCALES
(setf vocales '(a e i o u))
vocales ; => '(A E I O U)
```

> Puede que sea necesario crear una variable con `defvar` antes de asignarla, esto dependerá del "intérprete" de LISP que se use.

## La función `let`

`let` es una macro que nos permite crear variables, suele ser usada únicamente para variables locales. Su sintaxis es:

```lisp
(let ((variable-1 valor-1)
      (variable-2 valor-2)
      ...
      (variable-n valor-n))
 cuerpo)
```

Ejemplo:

```lisp
(defun lanzamiendo-de-moneda ()
 (let ((resultado (random 101))
       (cond
	((< resultado 50) 'águila)
	((> resultado 50) 'sol)
	(t 'borde-de-la-moneda))))
 ```

 Nótese que esta sería otra forma de escribir la misma función, pero con errores en la programación, pues cada ocasión que comparamos valores se genera un nuevo número, por lo que puede ocurrir el caso en el que todas las condiciones se evalúen en `NIL` y obtengamos como resultado final `NIL`:

 ```lisp
 (defun lanzamiendo-de-moneda ()
  (cond
   ((< (random 101) 50) 'águila)
   ((> (random 101) 50) 'sol)
   ((equal (random 101) 50) 'borde-de-la-moneda)))
 ```

## La función `let*`

 `let*` por otra parte crea variables una a la vez (a diferencia de `let` que crea y asigna los valores a las variables al mismo tiempo), la primera variable local forma parte de un _contexto_ en el cual se calcula el valor de la segunda variable y así sucesivamente para el resto de las variables.

Ejemplo:

```lisp
(defun diferencia-de-precio (precio-antiguo precio-actual)
 (let* ((diferencia (- precio-actual precio-antiguo))
	(proporcion (/ diferencia precio-antiguo))
	(porcentaje (* proporcion 100.0)))
  (list 'el 'artículo 'cambió 'en 'un porcentaje 'por 'ciento)))

(diferencia-de-precio 3 5) ; => (EL ARTÍCULO CAMBIÓ UN 66.66667 POR CIENTO)
```

Cambiar `let*` por `let` resultará en un error.
