; 12.1
; Describe los roles de los símbolos CAPITAN, :CAPITAN y STAR-SHIP-CAPTAIN

12.1
(defstruct Navespacial
    (capitan nil)
    (nombre nil)
    (escudos 'down)
    (condicion 'green)
    (velocidad 0))

; 12.2
; ¿Es (starship-p 'starship) verdadeo?
12.2
(setf 00WPI (make-Navespacial))
(navespacial-p 00WPI)

; 12.3
; ¿Cuáles son los valores de (type-of make-starship),
; (type-of 'make-starship) y (type-of (make-starship))?
12.3
(type-of 'make-navespacial)
;; Function

(type-of #'make-navespacial)
;; Navespacial

(type-of (navespacial-capitan))

; 12.5
; En este ejercicio se creará una red de diagnóstico de un automóvil.
;
; a)
; Escribir DEFSTRUCT para una estructura llamada NODO, con 4 componentes
; NOMBRE, PREGUNTA, CASO-SI y CASO-NO

(defstruct nodo
  (nombre nil)
  (pregunta nil)
  (caso-si nil)
  (caso-no nil))

; b)
; Definir una variable global *LISTA-DE-NODOS* que almacenará todos los
; nodos en la red. Escribe la función INICIALIZA que inicializa la red al poner
; *LISTA-DE-NODOS* igual a NIL.

(defvar *lista-de-nodos*)

(defun inicializa ()
  (setf *lista-de-nodos* ()))

; c)
; Escribe AGREGAR-NODO, debe regresar el nombre del nodo agregado.

(defun agrega-nodo (nombre pregunta caso-si caso-no)
  (push (make-nodo
         :nombre nombre
         :pregunta pregunta
         :caso-si caso-si
         :caso-no caso-no) *lista-de-nodos*)
  (nodo-nombre (car *lista-de-nodos*)))

; d)
; Escribe ENCUENTRA-NODO, que toma el nombre de un nodo como entrada y
; devuelve el nodo si este aparece en *LISTA-DE-NODOS*, NIL en otro caso.

(defun encuentra-nodo (nombre)
  (dolist (i *lista-de-nodos*)
    (when (eq (nodo-nombre i) nombre)
      (return i))))

; e)
; Escribe PROCESAR-NODO. Recibe un nodo como entrada, si lo encuentra realiza
; la pregunta asociada a ese nodo y devuelve la acción CASO-SI o CASO-NO
; dependiendo de la respuesta. Si no se encontró el nodo se devuelve NIL.

(defun procesa-nodo (nombre)
  (let ((nd (encuentra-nodo nombre)))
    (if nd
        (if (y-or-n-p "~&~A?: " (nodo-pregunta nd))
            (nodo-caso-si nd)
            (nodo-caso-no nd))
        (format t "~&El nodo ~S aún no ha sido definido." nombre))))

(agrega-nodo 'arrancar
          "El motor logra arrancar?"
          'motor-enciende
          'motor-no-enciende)

(agrega-nodo 'motor-enciende
          "El motor se usará por un periodo de tiempo indefinido?"
          'motor-usado-brevemente
          'motor-no-enciende)

(agrega-nodo 'motor-no-enciende
          "¿Se escucha un sonido al girar la llave?"
          'sonido-al-girar-llave
          'sin-sonido-al-girar-llave)

(agrega-nodo 'sin-sonido-al-girar-llave
          "¿La batería está baja?"
          "Reemplaza la batería"
          'voltaja-bateria-bien)

(agrega-nodo 'voltaja-bateria-bien
          "¿Los cables de la batería están sucios o mal conectados?"
          "Limpiar los cables y ajustar las conexiones"
          'cables-bateria-bien)


; f)
; Escribe EJECUTAR. Esta asigna NODO-ACTUAL como variable local cuyo valor
; inicial es ARRANCAR. Realiza un bucle llamando PROCESAR-NODO para procesar
; el nodo y almacenar el valor retornado por PROCESAR-NODO en NODO-ACTUAL.
; Si el valor retornado es una cadena detenemos el bucle, si el valor es NIL
; también se detiene.

(defun ejecutar ()
  (let ((nodo-actual (make-nodo :nombre 'arrancar)))
  (setf nodo-actual (procesar-nodo nodo-actual))))

(defun ejecutar ()
  (do ((nodo-actual (make-nodo :nombre 'arrancar)))
      ((or (null nodo-actual) (stringp nodo-actual)) (return))
    (setf nodo-actual (procesar-nodo nodo-actual))))

; g)
; Escribe una función interactiva para crear un nuevo nodo. Debe de solicitar
; que se ingresen todos los componentes. La pregunta debe de ser una cadena,
; rodeada por dos comillas dobles. El nodo debe de ser agregado a la red.

(defun agrega-nodo-interactivo ()
  (format t "Ingresa el nombre del nodo: ")
  (setq nombre (read))
  (format t "Ingresa la pregunta del nodo: ")
  (setq pregunta (read-line))
  (format t "Ingresa el caso si del nodo: ")
  (setq caso-si (read-line))
  (format t "Ingresa el caso no del nodo: ")
  (setq caso-no (read-line))
  (setf *lista-de-nodos*
        (push
         (make-nodo :nombre nombre
                    :pregunta pregunta
                    :caso-si caso-si
                    :caso-no caso-no)
         *lista-de-nodos*)))

; h)
; Si el motor se ejecuta brevemente pero se detiene cuando está frío, es posible
; que las revoluciones por minuto (idle rpm) tengan un valor muy bajo. Escribe
; un nodo llamado motor-funciona-breve para indicar que el motor se detenga
; cuando está frío pero no caliente. Luego agrega otro nodo a la red donde se
; pregunta si la velocidad idle fría es al menos 700 rpm, si no lo es, pedir
; al usuario ajustar la velocidad idle.
