# Iteración y estructura de bloques

En este capítulo se analiza un enfoque distinto a la recursión, la iteración.
Se analiza el uso de macros como `DOTIMES`, `DOLIST`, `DO` y `DO*`. Finalmente
definimos qué son los bloques implícitos y cómo usar `RETURN-FROM` para
salir de ellos.

## `DOTIMES` y `DOLIST`
Estas son las macros iterativas más simples, lo que significa que no evalúan
todos sus argumentos (como `OR`). La sintaxis es la misma para ambas:

```lisp
(DOTIMES (variable-índice n [result-form])
    cuerpo)

(DOLIST (variable-índice lista [forma-resultado])
    cuerpo)
```

Por ejemplo:

```lisp
(DOTIMES (contador 3)
    (format t "~&Despegando en ~S..." contador))

(DOLIST (elemento '(a b c))
    (format t "~&Iterando en ~S" elemento))
```

Para estas macros, `result-form` (`forma-resultado`) es un valor que se devuelve
al finalizar la ejecución, si no es especificada se devolverá `NIL`.

## Saliendo de un bucle

La función `RETURN` puede ser empleada para salir inmediatamente de un bucle,
evitando seguir evaluando las instrucciones restantes. Al hacer uso de `RETURN`
la `forma-resultado` (`result-form`) del bucle se ignora en caso de que exista.

La función `encuentra-primer-impar` ejemplifica lo anterior:

```lisp
(defun encuentra-primer-impar (lista-de-numeros)
    (dolist (numero lista-de-numeros)
        (format t "~&Evaluando el número ~S..." numero)
        (when (oddp numero)
            (format t "~&¡Encontré un número impar!")
            (return numero))))
```

Igualmente podemos simular comportamientos de macros como `OR` o estructuras
recursivas implementando el retorno de valores con bucles:

```lisp
(defun verifica-todos-pares (lista-de-numeros)
    (dolist (numero lista-de-numeros)
        (format t "Evaluando ~S..." numero)
        (if (not (even p)) (return nil))))
```

En caso de que uno de los valores sea impar se devolverá `NIL` y la ejecución
del bucle finalizará.

## Comparando búsqueda recursiva e iterativa

Generalmente, cuando se trata de listas planas (listas sin listas dentro
de ellas), la iteración será más simple que la recursión (dependiendo de la
implementación). Tomemos en cuenta este código:

```lisp
(defun rec-ejemplo (lista)
    "Encuentra el primer impar en una lista usando recursión"
    (cond ((null lista) nil)
          ((oddp (first lista)) (first lista))
          (t (rec-ejemplo (rest lista)))))

(defun it-ejemplo (lista)
    "Encuentra el primer impar en una lista usando iteración"
    (dolist (e lista)
        (if (oddp e) (return e))))
```

Notemos que para la versión iterativa se entiende claramente dónde finaliza la
ejecución del bucle, además de que el elemento a iterar se denota con la
variable `e`, mientras que en la versión recursiva es inicialmente un poco más
complicado entender la función (pero no es más elegante).

Es importante mencionar que hay otras situaciones donde la recursión puede ser
más simple de implementar y comprender, por ejemplo, recorrer un árbol.

## Construyendo resultados con asignaciones

De igual manera que con las llamadas recursivas, es posible construir elementos
(como listas) con cada iteración.

En este código creamos la función factorial usando `DOTIMES`, que asigna un
valor a la variable `producto` antes de comenzar el bucle, de esta forma, al
pedir el factorial de `0` nos aseguraremos de devolver el valor adecuado.
Posteriormente sumamos `1` a la variable `i` que cambia con cada iteración,
pues, inicialmente vale `0`, si no se realiza esta suma el producto valdría
`0`. Al terminar `DOTIMES` nos aseguramos de devolver `producto`, pues es el
tercer argumento de esta función, es decir la `forma-resultado` (`result-form`).

```lisp
(defun factorial-iterativo (n)
    (let ((producto 1))
        (dotimes (i n producto)
            (setf producto (* producto (+ i 1))))))
```

Este otro fragmento devuelve la intersección de dos listas. Iniciamos
declarando `conjunto-resultado` donde almacenaremos los elementos
que están en `x` e `y`, luego iteramos en cada elemento de `x` para
verificar si también es miembro de `y`, en caso de que lo sea usamos
`push` para insertar ese elemento en la lista (conjunto) que 
se devolverá.

```lisp
(defun interseccion-iterativa (x y)
    (let ((conjunto-resultado nil))
        (dolist (elemento x conjunto-resultado)
            (when (member elemento y)
                (push elemento conjunto-resultado)))))
```

## Comparando `DOLIST` contra `MAPCAR` y recursión

Si intentamos replicar la siguiente función

```lisp
(defun aplica-cuadrado-a-lista (lista-de-numeros)
    (mapcar #'(lambda (n) (* n n)) lista-de-numeros))
```

En su forma iterativa:

```lisp
(defun erronea-cuadrado-a-lista-iterativa (lista-de-numeros)
    (let ((lista-resultado nil))
        (dolist (elemento lista-de-numeros lista-resultado)
            (push (* elemento elemento) lista-resultado))))
```

Notaremos que al ejecutar la segunda función obtendremos la lista en orden
invertido, esto se debe a `PUSH`, pues al encontrar el primer elemento este
es ingresado al inicio de la lista.

Una solución sencilla es la siguiente:

```lisp
(defun cuadrado-a-lista-iterativa (lista-de-numeros)
    (let ((lista-resultado nil))
        (dolist (elemento lista-de-numeros (reverse lista-resultado))
            (push (* elemento elemento) lista-resultado))))
```

En esta versión nos aseguramos de respetar el orden al devolver el
inverso de la lista previamente invertida (?).

## La macro `DO`

`DO` es la iteración más compleja de Lisp, pues puede asignar cualquier número de
variables (como `LET`), tener múltiples variables índice y permite especificar
una _prueba_ para decidir cuándo salir del bucle. Esta es la sintaxis:

```lisp
(do ((var1 valor_inicial1 [actualizacion1])
     (var2 valor_inicial2 [actualizacion2])
        ...)
    (prueba acción-1 ... acción-n)
    cuerpo)
```

La forma en la que `DO` funciona es la siguiente:

1. Todas las variables son asignadas a su valor inicial
2. Se evalúa prueba
   - Si devuelve verdadero, `DO` evalúa las acciones
   - Si devuelve falso ejecuta el código del cuerpo
     - Nota: El cuerpo puede contener llamadas a `RETURN` para poder salir de
     la iteración inmediatamente.
3. Se actualiza cada variable mediante la expresión `actuzalizacion` (en caso
   de haberla).
4. Nuevamente se verifica la prueba, si el resultado es verdad se ejecutan las
   acciones, en otro caso se continúa con la evaluación del cuerpo nuevamente.
   
Esta es una versión alternativa a `LANZAMIENTO`:

```lisp
(defun lanzamiento (n)
    (do ((contador n (- contador 1)))
        ((zerop contador) (format t "¡Despegando!"))
    (format t "~&~S..." contador)))
```

## Ventajas de la asignación implícita

A diferencia de `DOTIMES` y `DOLIST`,`DO` tiene ventajas como:

1. Modificar las variables índice en cualquier valor (incluso en valores negativos) 
2. Poder asignar múltiples variables (evitando usar `LET` y `SETF`).

```lisp
(defun factorial (n)
    (do ((i n (- i 1))
         (resultado 1 (* resultado i)))
        ((zerop i) resultado)))
```

La función de arriba es la versión de factorial implementada usando `DO`.
Esta hace uso de la asignación "paralela" mientras que las variables
iterativas cambian sus valores de mayor a menor en lugar de menor a mayor
como en `DOTIMES`.

Cuando calculamos `(factorial 5)` obtenemos los siguientes valores para
cada variable:

| I | RESULTADO | (- I 1) | (* RESULTADO I) |
|---|:---------:|:-------:|:---------------:|
| 5 |     1     |    4    |        5        |
| 4 |     5     |    3    |        20       |
| 3 |     20    |    2    |        60       |
| 2 |     60    |    1    |       120       |
| 1 |    120    |    0    |       120       |
| 0 |    120    |         |                 |

Notemos que `CUENTA-REBANADAS` y `FACTORIAL` presentan "cuerpos" vacíos,
gracias a que las asignaciones iniciales y las expresiones de
actualización para cada variable se encargan de hacer todo el trabajo.
Bastante elegante, ¿cierto?

Hay casos donde realizar todo el trabajo en las secciones de actualización
y no en el cuerpo complica las cosas, como el siguiente:

```lisp
(defun interseccion-iterativa (x y)
    (do ((x1 x (rest x1))
         (resultado nil (if (member (first x1) y)
                            (cons (first x1) resultado)
                            resultado)))
        ((null x1) resultado)))
```

Con cada iteración se actualiza el valor de `RESULTADO`, se haya
encontrado o no una coincidencia, esto es algo innecesario y con una
sintaxis difícil de comprender. Podemos escribir una versión más simple
al omitir la expresión de `resultado` en la lista de variables y
reemplazándola por una condicional en el cuerpo que ejecuta `PUSH`:

```lisp
(defun interseccion-iterativa (x y)
    (do ((x1 x (rest x1))
         (resultado nil))
        ((null x1) resultado)
      (when (member (first x1) y)
            (push (first x1) resultado))))
```

Podemos decir que `DOLIST` es más útil de emplear cuando _sólo_
necesitamos iterar sobre los elementos de una lista, aunque `DO` es más
general. Por ejemplo, podemos iterar sobre dos listas con `DO` al mismo
tiempo, para encontrar una coincidencia entre ambas:

```lisp
(defun elemento-coincidencia (x y)
    "Busca en X y Y el primer elemento que coincida"
    (do ((x1 x (rest x1))
         (y1 y (rest y1)))
        ((or (null x1) (null y1)) nil)
      (if (equal (first x1)
                 (first y1))
          (return (first x1)))))
```

## La macro `*DO`

La siguiente función está escrita empleando `DO`. Sigue las convenciones
que ya conocemos, se itera sobre la variable `X` y el resto de sus
elementos. En el cuerpo, nos referimos al primer elemento de esta lista `X`
con `(FIRST X)`.

```lisp
(defun epi-con-do (lista)
  "Encuentra Primer Impar con DO"
  (do ((x lista (rest x)))
      ((null x) nil)
    (if (oddp (first x)) (return (first x)))))
```

De forma equivalente a `LET` y `LET*`, `DO*` asigna de manera secuencial
las variables en lugar de todas al mismo tiempo como en `DO`. La sintaxis
sigue siendo la misma. Una ventaja de `DO*` en la función
`ENCUENTRA-PRIMER-IMPAR` es que nos permite definir una segunda varaible
índice que almacena mientras que la primera almacena:

```lisp
(defun epi-con-do* (lista)
  (do* ((x lista (rest x))
        (e (first x) (first x)))
       ((null x) nil)
    (if (oddp e) (return e))))
```

En esta última función tenemos la variable que itera sobre la lista (`X`)
y la variable dependiente que representa el primer elemento de ésta (`E`).

## Bucles infinitos con `DO`

Si necesitamos entrar en bucles infinitos basta con especificar `NIL`
como prueba de finalización. Esto puede ser útil cuando se trata de
leer algo específico del teclado, como un número. Si lo ingresado
no es un número se sigue intentado, cuando la entrada sea la deseada
la función "rompe" el bucle con `RETURN`, devolviendo así el número.

```lisp
(defun leer-numero ()
  (do ((respuesta nil))
      (nil)
    (format t "~&Ingresa un número: ") (setf respuesta (read))
    (if (numberp respuesta) (return respuesta))
    (format t
            "~&Ingresa un valor válido. Intenta nuevamente"
            respuesta)))
```

## Bloques implícitos

Los cuerpos de las funciones están contenidos en bloques implícitos,
donde el nombre de la función funge como el **nombre de bloque**.
Un bloque es una secuencia de expresiones de las que se puede
salir en cualquier punto mediante la función especial `RETURN-FROM`.

Los argumentos de `RETURN-FROM` son un nombre de bloque y una
expresión resultado, dado que el nombre del bloque se evalúa no es
necesario "quotearlo" (`quote`).

```lisp
(defun hallar-primer-impar (lista)
  (format t "~&Buscando un impar...")
  (dolist (elemento lista)
    (when (oddp elemento)
      (format t "~&Encontré ~S" elemento)
      (return-from hallar-primer-impar elemento)))
  (format t "~&No hubo coincidencias.")
  'none)
```

En el código de arriba usamos `RETURN-FROM` para salir del
cuerpo de la función con el nombre de bloque indicado, no
solo de `DOLIST`. De hecho, `DO`, `DO*`, `DOLIST` y
`DOTIMES` se encuentran rodeados de bloques implícitos con
el nombre de `NIL`. La expresión `(RETURN X)` es una mera
abreviación de `(RETURN-FROM NIL X)`. De esta forma, en
el cuerpo de la función `HALLAR-PRIMER-IMPAR` `RETURN-FROM`
está anidada dentro de un bloque llamado `NIL`, que a su vez
está contenido en un bloque llamado `HALLAR-PRIMER-IMPAR`.

Este es otro ejemplo que involucra `RETURN-FROM` sin involucrar
un enfoque iterativo. En caso de que la función reciba un
argumento que no es un número se devuelve el símbolo `NOP`
en lugar de obtener un error. `RETURN-FROM` no sólo sale de la
expresión lambda, también de `MAPCAR` y el cuerpo de la función
`LISTA-CUADRADO` incluso.

```lisp
(defun lista-cuadrado (lista)
  (mapcar
   #'(lambda (e)
       (if (numberp e) (* e e)
           (return-from lista-cuadrado 'nop)))
   lista))
```

> También es posible definir bloques implícitos usando la función
> especial `BLOCK`
