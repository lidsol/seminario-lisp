; 11.1
(defun member-iterativa (x lista)
  "Devuelve T si X es miembro de LISTA"
  (dolist (i lista)
    (when (equal i x)
      (return T))))

(member-iterativa 'b '(a b c)) ; => T
(member-iterativa 'd '(a b c)) ; => NIL

; 11.2
(defun assoc-iterativa (llave lista)
  "Versión iterativa de assoc"
  (dolist (elemento lista)
    (when (equal (first elemento)
                 llave)
      (return elemento))))

(assoc-iterativa 'two numeros) ; => (TWO DOS)

; 11.3
(defun todos-pares-p (lista)
  "Verifica si la LISTA es de números pares"
  (dolist (numero lista T)
    (when (oddp numero)
      (return NIL))))

(todos-pares-p '(2 2 3))
(todos-pares-p '(2 2 4))

; 11.4
(defun length-iterativa (lista)
  "Versión iterativa de LENGTH"
  (let ((total 0))
    (dolist (elemento lista total)
      (setf total (1+ total)))))

(length-iterativa '(a b c))

; 11.5
; Escrive una versión iterativa de NTH, IT-NTH
(defun nth-iterativo (n lista)
  (let ((acumulador 0))
    (dolist (i lista)
      (when (equal acumulador n)
        (return i))
      (setf acumulador (1+ acumulador)))))

; 11.6
; Escribe una versión alternativa de UNION, IT-UNION
; Asegúrate de devolver el resultado en el mismo orden que UNION
; NOTA: Para que `push' funcione es necesario inicializar una variable en nil
; NOTA2: No me importa el orden :p
; TODO: Mejorar esto
(defun union-iterarivo (lista_1 lista_2)
  (setf resultado nil)
  (dolist (n lista_1 lista_2)
    (if (not (member n lista_2))
        (push n lista_2))))
; 11.7
; ¿Por qué IT-INTERSECCION devolvió la lista invertida?
; Intenta solucionar este inconveniente.
(defun it-interseccion (x y)
  (let ((conjunto-resultado nil))
    (dolist (elemento x (reverse conjunto-resultado))
      (when (member elemento y)
        (push elemento conjunto-resultado)))))

(it-interseccion '(f a c e) '(c l o v e)) ; => (C E)

; 11.8
; Escribe una versión iterativa de REVERSE, IT-REVERSE
(defun it-reverso (lista-orig)
  (let ((lista nil))
    (dolist (elemento lista-orig lista)
          (push elemento lista))))

; 11.9
; Escribe VERIFICA-TODOS-IMPARES usando DO
(defun verifica-todos-impares (lista)
  (do ((l lista (rest l)))
      ((null l) t)
    (when (evenp (first l))
      (return nil))))

; 11.10
; Escribe LANZAMIENTO usando DOTIMES
(defun lanzamiento-dotimes (n)
  (dotimes (i n)
    (format t "~&~S..." (- n i)))
  (format t "~&¡Despegando!"))

    "Return the first non-integer element
(defun first-non-integer (x)
  (do* ((z x (rest z))) 
)
