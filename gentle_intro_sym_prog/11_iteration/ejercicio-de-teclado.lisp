(defvar bases '((a t) (t a) (c g) (g c)))

(defun base-complementaria (base)
  "Regresa la base complementaria."
  (cadr (assoc base bases)))

(defun hebra-complementaria (hebra)
  "Regresa la hebra complementaria."
  (let ((comp ()))
	(dolist (base hebra (reverse comp))
	  (push (base-complementaria base) comp))))

(defun hacer-doble (hebra)
  "Regresa la versión doble de la hebra."
  (let ((doble ()))
	(dolist (base hebra (reverse doble))
	  (push (list base (base-complementaria base)) doble))))

(defun cuenta-bases (hebra)
  "Regresa una tabla con el número de apariciones de cada base."
  (do ((a 0)
	   (t 0)
	   (c 0)
	   (g 0))
	  ((null (cdr hebra)) `((a ,a) (t ,t) (c ,c) (g ,g)))
	(cond ((listp (car hebra)) (auxiliar-cuenta-bases (caar hebra) (auxiliar-cuenta-bases (cadar hebra))))
		  (t (auxiliar-cuenta-bases (car hebra))))))
;d. intento 1
(defun auxiliar-cuenta-bases (args)
  (cond ((equal hebra a) (incf a))
				 ((equal hebra t) (incf t))
				 ((equal hebra c) (incf c))
				 ((equal hebra g) (incf g))))

;d. Intento final
(defun contar-base (hilo)
  (do ((x hilo (rest x))
    (A 0) ;ACTUALIZAR CADA VEZ QUE SE ENCUENTRE A EN X
    (Ti 0) ;ACTUALIZAR CADA VEZ QUE SE ENCUENTRE B EN X
    (C 0) ;ACTUALIZAR CADA VEZ QUE SE ENCUENTRE C EN X
    (G 0)) ;ACTUALIZAR CADA VEZ QUE SE ENCUENTRE G EN X
    ;salida
    ((null x) (format t "A vale ~S, T vale ~S, G vale ~S, C vale ~S." A Ti G C))
    ;cuerpo
    (COND ((equal (first x) 'A)
            (incf A))
       ((equal (first x) 'Ti)
            (incf Ti))
       ((equal (first x) 'C)
           (incf C))
       ((equal (first x) 'G)
           (incf G))
) ) )

; e
(defun prefijop (hebra1 hebra2)
  "Devuelve T si hebra1 está al inicio de hebra2"
  (do
   ((lista1 hebra1 (cdr lista1))
    (lista2 hebra2 (cdr lista2)))
   ((null lista1) t)
    (unless (equal (first lista1)
                   (first lista2))
      (return nil))))

(prefijop '(G T C) '(G T C A T)) ; => T
(prefijop '(G T C) '(G C C A T)) ; => NIL

; f
(defun aparecep (hebra1 hebra2)
  "Devuelve T si hebra1 aparece en alguna parte de hebra2"
  (do
   ((lista1 hebra1)
    (lista2 hebra2 (rest lista2)))
   ((null lista2) nil)
    (when (prefijop lista1 lista2)
      (return t))))

(aparecep '(G A T) '(A G A T A)) ; => T
(aparecep '(G A T) '(A G C T A)) ; => NIL

; g
(defun cubrep (hebra1 hebra2)
  "Devuelve T si al repetir hebra1 N veces obtenemos hebra2"
  (do
   ((lista2 hebra2 (nthcdr (length hebra1) lista2)))
   ((null lista2) t)
    (when (null (prefijop hebra1 lista2))
      (return nil))))

(cubrep '(A T) '(A T A T)) ; => T
(cubrep '(A T) '(A T A G)) ; => NIL
(cubrep '(A T) '(A T A T A)) ; => NIL

; h
(defun prefijo (n hebra)
  "Devuelve los primeros n atomos de hebra"
  (do
   ((i n (1- i))
    (lista1 hebra (rest lista1))
    (lista2 '()))
   ((zerop i) (reverse lista2))
    (push (first lista1) lista2)))

(prefijo 2 '(C G A T T A G)) ; => (C G)
(prefijo 4 '(C G A T T A G)) ; => (C G A T)

; i - Quizá no sea la función más elegante :P
(defun kernel (hebra)
  "Devuelve el prefijo mínimo para cubrir hebra"
  (dotimes (i (length hebra))
    (when (cubrep (prefijo (1+ i) hebra) hebra)
      (return (prefijo (1+ i) hebra)))))

(kernel '(a g c a g c a g c)) ; => (A G C)
(kernel '(t a c a)) ; => (T A C A)

; j - En proceso
;(defun dibuja-hebra (hebra)
;  "Dibuja una hebra y su complemento"
;  (format t "~&~S" hebra))

;
;(defun dibujar-adn (hebra)
;    (do (ade hebra );Variables
;        ()
;        ();salida
;
;        (cond ()
;        );Cuerpo
;    )
;)
;j
;(defun dibuja-adn (hilo)
;  (do ((x hilo (rest x)))
;    ;salida
;    ((null x) 0)
;    ;cuerpo
;    (format t "")
;    (COND ((equal (first x) 'A)
;            (format t "  .  ~&  T  ~&  !  ~&_____"))
;            ;(format t "  T  ~&")
;            ;(format t "  !  ~&")
;            ;(format t "_____"))
;        ((equal (first x) 'T)
;            (format t "  .  ~&  A  ~&  !  ~&_____~&"))
;            ;(format t "  A  ~&")
;            ;(format t "  !  ~&")
;            ;(format t "_____~&"))
;          ((equal (first x) 'G)
;                  (format t "  .  ~&")
;                  (format t "  C  ~&")
;                  (format t "  !  ~&")
;                  (format t "_____~&"))
;            ((equal (first x) 'C)
;              (format t "  .  ~&")
;              (format t "  G  ~&")
;              (format t "  !  ~&")
;              (format t "_____~&"))
;) ) )

(defun dibuja-adn (hebra)
(let ((n (length hebra)))
     (dibuja-hilo n "-----")
     (dibuja-hilo n "  !  ")
     (dibuja-bases hebra)
     (dibuja-hilo n "  .  ")
     (dibuja-hilo n "  .  ")
     (dibuja-bases (hebra-complementaria hebra))
     (dibuja-hilo n "  !  ")
     (dibuja-hilo n "-----")))

(defun dibuja-hilo (cnt hilo)
    (format t "~&")
    (dotimes (i cnt)
             (format t "~A" hilo)))

(defun dibuja-bases (hebra)
    (format t "~&")
    (dolist (base hebra)
            (format t "  ~A  " base)))
