;; versión con DOTIMES
(defun smdtyc (limite)
  "Suma los multiplos de 3 y 5 debajo de limite"
  (let ((resultado 0))
    (dotimes (numero limite resultado)
      (if (or (equal (mod numero 3) 0)
              (equal (mod numero 5) 0))
          (setf resultado (+ resultado numero))))))

(smdtyc 1000) ; => 233168 (18 bits, #x38ED0)

;; versión con DO y cuerpo
(defun suma-multiplos-3-y-5 (n)
  (do ((i (- n 1) (- i 1))
       (resultado 0))
      ((zerop i) resultado)
    (if (or (equal (mod i 3) 0)
            (equal (mod i 5) 0))
      (setf resultado (+ resultado i)))))

(suma-multiplos-3-y-5 1000) ; => 233168 (18 bits, #x38ED0)

;; versión con DO y sin cuerpo
(defun multiplos (n)
  (do ((i 1 (1+ i))
       (suma 0
             (if (or (equal 0 (mod i 3))
                     (equal 0 (mod i 5)))
                 (+ suma i) suma)))
      ((equal i n) suma)))

(multiplos 1000) ; => 233168 (18 bits, #x38ED0)
