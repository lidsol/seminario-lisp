# Seminario de LISP

En este seminario recuperaremos diversos materiales para aprender las bondades
y retos de la familia de lenguajes LISP. En un principio nos enfocamos en la
programación empleando `scheme`, un dialecto minimalista y elegante de la familia
de lenguajes de programación LISP. Actualmente revisamos el dialecto Common Lisp.

Nos reunimos tres días a la semana; lunes, miercoles y jueves en un horario de 19:00 a
20:00 (Tiempo de la Ciudad de México) en la sala de Jitsi
[SeminarioLispLisdol](https://meet.jit.si/seminariolisplidsol).

## Materiales discutidos

* [x] *Structure and Interpretation of Computer Programs*: El libro SICP fue
	utilizado en el MIT durante décadas para iniciar a sus alumnos a la
	computación y a la programación.
	* [x] Apartado 1.1 *The Elements of Programming*
* [x] The Little Schemer
	* [x] Capítulos 1 a 8
* [ ] A Gentle Introduction to Symbolic Computation
	* [x] Capítulos 1 a 12
* [ ] Practical Common Lisp
* [ ] ANSI Comon Lisp
* [ ] The Seasoned Schemer 
* [ ] The Reasonde Schemer

## Temas adicionales de interés

* [ ] Introducción a quicklisp y uso de paquetes en LISP (por definir)
* [ ] Procesamiento de "Enegramas" (PNL).
